SELECT 
	a.id, 
	c.masv,
	d.ten,
	d.email, 
	d.dienthoai, 
	h.ten as tentruong, 
	i.ten as tenkhoa,
	b.nienkhoa,
	f.ten as tengiuong, 
	g.ten as tenphong,
	SUM(e.tiennothanghientai) as tongtienno
FROM 
	tblhopdong a, 
	tblsinhvienkhoa b, 
	tblsinhvien c, 
	tblnguoi d, 
	tblhoadon e, 
	tblgiuong f, 
	tblphong g, 
	tbltruong h, 
	tblkhoa i
WHERE 
	a.id IN (
		SELECT tblHopdongId 
		FROM tblhoadon 
		WHERE trangthai IN (1,2)
	) 
	AND a.tblSinhvienKhoaId = b.id 
	AND e.trangthai IN (1,2)
	AND a.id = e.tblHopdongId
	AND b.tblSinhvienId = d.id 
	AND c.tblNguoiId = d.id 
	AND a.tblGiuongId = f.id 
	AND f.tblPhongId = g.id 
	AND i.id = b.tblKhoaId 
	AND i.tblTruongId = h.id
GROUP BY a.tblSinhvienKhoaId
;


SELECT 
	e.ten,
	d.kythanhtoan,
	d.tiennothanghientai, 
	d.ngayphaithanhtoan 
FROM 
	tblsinhvienkhoa b, 
	tblhopdong c, 
	tblhoadon d, 
	tblnguoi e
WHERE 
	c.tblSinhvienKhoaId = b.id
	AND b.tblSinhvienId = e.id
	AND d.trangthai IN (1,2)
	AND d.tblHopdongId = c.id
	AND e.id = 2
;


SELECT 
	a.kythanhtoan, 
	b.sotien, 
	c.ngaythanhtoan
FROM 
	tblhoadon a, tblthanhtoanhoadon b, tblthanhtoan c
WHERE
	a.id = 7
	AND a.id = b.tblHoadonId
	AND b.tblThanhtoanId = c.id
GROUP BY c.ngaythanhtoan;



DROP PROCEDURE IF EXISTS TKSVTheoDuno;
CREATE PROCEDURE TKSVTheoDuno()
BEGIN
	SELECT 
		a.id, 
		c.masv,
		d.hodem,
		d.ten,
		d.email, 
		d.dienthoai, 
		h.ten as tentruong, 
		i.ten as tenkhoa,
		b.nienkhoa,
		f.ten as tengiuong, 
		g.ten as tenphong,
		SUM(e.tiennothanghientai) as tongtienno
	FROM 
		tblhopdong a, 
		tblsinhvienkhoa b, 
		tblsinhvien c, 
		tblnguoi d, 
		tblhoadon e, 
		tblgiuong f, 
		tblphong g, 
		tbltruong h, 
		tblkhoa i
	WHERE 
		a.id IN (
			SELECT tblHopdongId 
			FROM tblhoadon 
			WHERE trangthai IN (1,2)
		) 
		AND a.tblSinhvienKhoaId = b.id 
		AND e.trangthai IN (1,2)
		AND a.id = e.tblHopdongId
		AND b.tblSinhvienId = d.id 
		AND c.tblNguoiId = d.id 
		AND a.tblGiuongId = f.id 
		AND f.tblPhongId = g.id 
		AND i.id = b.tblKhoaId 
		AND i.tblTruongId = h.id
	GROUP BY a.tblSinhvienKhoaId
	ORDER BY tongtienno DESC;
	
	END
