/*
 Navicat Premium Data Transfer

 Source Server         : Techsaku
 Source Server Type    : MySQL
 Source Server Version : 100506
 Source Host           : 139.180.154.73:3306
 Source Schema         : room_manage

 Target Server Type    : MySQL
 Target Server Version : 100506
 File Encoding         : 65001

 Date: 05/01/2021 17:36:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbldiachi
-- ----------------------------
DROP TABLE IF EXISTS `tbldiachi`;
CREATE TABLE `tbldiachi`  (
  `id` int(11) NOT NULL,
  `sonha` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `xompho` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phuongxa` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quanhuyen` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tinhthanh` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbldiachi
-- ----------------------------
INSERT INTO `tbldiachi` VALUES (1, '14 An Hòa', NULL, 'Mỗ Lao', 'Hà Đông', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (2, '98 Hoàng Quốc Việt', NULL, 'Nghĩa Đô', 'Cầu Giấy', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (3, '169 Phùng Chí Kiên', NULL, 'Nghĩa Đô', 'Cầu Giấy', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (4, '169 Tây Sơn', NULL, 'Quang Trung', 'Đống Đa', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (5, '97 HAO NAM', NULL, 'O CHO DUA', 'Đống Đa', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (6, '101 MAI HAC DE', NULL, 'BUI THI XUAN', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (7, '24B LO DUC', NULL, 'PHAM DINH HO', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (8, '19 TRAN QUOC HOAN', NULL, 'LANG HA', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (9, '30 HANG BONG', NULL, 'HANG GAI', 'HOAN KIEM', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (10, '3 NHA CHUNG', NULL, 'HANG TRONG', 'HOAN KIEM', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (11, '34 Cầu giấy', NULL, 'Mỗ Lao', 'Hà Đông', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (12, '48 Hoàng Quốc Việt', NULL, 'Nghĩa Đô', 'Cầu Giấy', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (13, '769 Phùng Chí Kiên', NULL, 'Nghĩa Đô', 'Cầu Giấy', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (14, '169 Tây Sơn', NULL, 'Quang Trung', 'Đống Đa', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (15, '95B Phố Huế', NULL, 'O CHO DUA', 'Đống Đa', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (16, '121 MAI HAC DE', NULL, 'BUI THI XUAN', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (17, '234B Cầu giấy', NULL, 'PHAM DINH HO', 'Lê Chân', 'Hải Phòng');
INSERT INTO `tbldiachi` VALUES (18, '382 Nguyễn Văn Cừ', NULL, 'LANG HA', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (19, '621D Cách Mạng Tháng 8', NULL, 'HANG GAI', 'HOAN KIEM', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (20, '95B Phố Huế', NULL, 'HANG TRONG', 'HOAN KIEM', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (21, '34 Cầu giấy', NULL, 'Mỗ Lao', 'Hà Đông', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (22, '122 Thái Hà', NULL, 'Nghĩa Đô', 'Cầu Giấy', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (23, '769 Phùng Chí Kiên', NULL, 'Nghĩa Đô', 'Cầu Giấy', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (24, '169 Tây Sơn', NULL, 'Quang Trung', 'Đống Đa', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (25, '95B Phố Huế', NULL, 'O CHO DUA', 'Đống Đa', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (26, '121 MAI HAC DE', NULL, 'BUI THI XUAN', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (27, '234B Cầu giấy', NULL, 'PHAM DINH HO', 'Lê Chân', 'Hải Phòng');
INSERT INTO `tbldiachi` VALUES (28, '19 TRAN QUOC HOAN', NULL, 'LANG HA', 'Hai Bà Trưng', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (29, '28 Trần phú', NULL, 'HANG GAI', 'HOAN KIEM', 'Hà Nội');
INSERT INTO `tbldiachi` VALUES (30, '55 Trần Quang Khải', NULL, 'HANG TRONG', 'HOAN KIEM', 'Hà Nội');



-- ----------------------------
-- Table structure for tbldichvu
-- ----------------------------
DROP TABLE IF EXISTS `tbldichvu`;
CREATE TABLE `tbldichvu`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dongia` float(255, 0) NULL DEFAULT NULL,
  `donvi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbldichvu
-- ----------------------------
INSERT INTO `tbldichvu` VALUES (1, 'Điện', 3500, NULL,'KW');
INSERT INTO `tbldichvu` VALUES (2, 'Nước', 15000, NULL,'Khối');
INSERT INTO `tbldichvu` VALUES (3, 'Xe máy', 50000, NULL,'Chiếc');
INSERT INTO `tbldichvu` VALUES (4, 'Xe đạp', 20000, NULL,'Chiếc');


-- ----------------------------
-- Table structure for tbldichvuphong
-- ----------------------------
DROP TABLE IF EXISTS `tbldichvuphong`;
CREATE TABLE `tbldichvuphong`  (
  `id` int(11) NOT NULL,
  `sodau` int(11) NULL DEFAULT NULL,
  `socuoi` int(11) NULL DEFAULT NULL,
  `soluongPhongSudung` int(11) NULL DEFAULT NULL,
  `tblPhongId` int(11) NOT NULL,
  `tblDichvuId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for tblhopdong
-- ----------------------------
DROP TABLE IF EXISTS `tblhopdong`;
CREATE TABLE `tblhopdong`  (
  `id` int(11) NOT NULL,
  `tiencoc` float(255, 0) NOT NULL,
  `giathue` float(255, 0) NOT NULL,
  `ngaybatdau` date NOT NULL,
  `tblSinhvienKhoaId` int(11) NOT NULL,
  `tblNhanvienId` int(11) NOT NULL,
  `tblGiuongId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblhopdong
-- ----------------------------

-- ----------------------------
-- Table structure for tblhopdongdichvu
-- ----------------------------
DROP TABLE IF EXISTS `tblhopdongdichvu`;
CREATE TABLE `tblhopdongdichvu`  (
  `id` int(11) NOT NULL,
  `tblDichvuId` int(11) NOT NULL,
  `tblHopdongId` int(11) NOT NULL,
  `soluong` int(11) NULL DEFAULT NULL,
  `dongia` float(255, 0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblhoadondichvu
-- ----------------------------
DROP TABLE IF EXISTS `tblhoadondichvu`;
CREATE TABLE `tblhoadondichvu`  (
  `id` int(11) NOT NULL,
  `tblHopdongDichvuId` int(11) NOT NULL,
  `soluongSudung` int(11) NOT NULL,
  `thanhtien` float(255, 0) NOT NULL,
  `tblHoadonId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblhoadondichvu
-- ----------------------------
INSERT INTO `tblhoadondichvu` VALUES (1,1,20,20*3500,1);
INSERT INTO `tblhoadondichvu` VALUES (2,2,5,5*15000,1);
INSERT INTO `tblhoadondichvu` VALUES (3,3,1,1*50000,1);
INSERT INTO `tblhoadondichvu` VALUES (4,4,1,1*20000,1);

INSERT INTO `tblhoadondichvu` VALUES (5,1,15,15*3500,2);
INSERT INTO `tblhoadondichvu` VALUES (6,2,6,6*15000,2);
INSERT INTO `tblhoadondichvu` VALUES (7,3,1,1*50000,2);
INSERT INTO `tblhoadondichvu` VALUES (8,4,1,1*20000,2);

INSERT INTO `tblhoadondichvu` VALUES (9,1,18,18*3500,3);
INSERT INTO `tblhoadondichvu` VALUES (10,2,7,7*15000,3);
INSERT INTO `tblhoadondichvu` VALUES (11,3,1,1*50000,3);
INSERT INTO `tblhoadondichvu` VALUES (12,4,1,1*20000,3);

INSERT INTO `tblhoadondichvu` VALUES (13,1,20,20*3500,4);
INSERT INTO `tblhoadondichvu` VALUES (14,2,3,3*15000,4);
INSERT INTO `tblhoadondichvu` VALUES (15,3,1,1*50000,4);
INSERT INTO `tblhoadondichvu` VALUES (16,4,1,1*20000,4);

INSERT INTO `tblhoadondichvu` VALUES (17,1,14,14*3500,5);
INSERT INTO `tblhoadondichvu` VALUES (18,2,4,4*15000,5);
INSERT INTO `tblhoadondichvu` VALUES (19,3,1,1*50000,5);
INSERT INTO `tblhoadondichvu` VALUES (20,4,1,1*20000,5);



-- ----------------------------
-- Table structure for tblhoadon
-- ----------------------------
DROP TABLE IF EXISTS `tblhoadon`;
CREATE TABLE `tblhoadon` (
  `id` int(11) NOT NULL,
  `tblHopdongId` int(11) NOT NULL,
  `tienthanghientai` float(255,0) NOT NULL,
  `kythanhtoan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tongnocu` float(255,0) DEFAULT NULL,
  `ngayphaithanhtoan` date NOT NULL,
  `trangthai` tinyint(3) NOT NULL COMMENT '1: Chưa thanh toán 2: Thanh toán thiếu  3: Đã thanh toán tháng hiện tại 4: Đã thanh toán',
  `tiennothanghientai` float(255,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tblthanhtoan
-- ----------------------------
DROP TABLE IF EXISTS `tblthanhtoan`;
CREATE TABLE `tblthanhtoan` (
  `id` int(11) NOT NULL,
  `sotien` float(255,0) NOT NULL,
  `ngaythanhtoan` datetime NOT NULL,
  `tblSinhvienId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tblthanhtoanhoadon
-- ----------------------------
DROP TABLE IF EXISTS `tblthanhtoanhoadon`;
CREATE TABLE `tblthanhtoanhoadon` (
  `id` int(11) NOT NULL,
  `sotien` float(255,0) NOT NULL,
  `tblHoadonId` int(11) NOT NULL,
  `tblThanhtoanId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
-- ----------------------------
-- Records of tblhoadon
-- ----------------------------


-- ----------------------------
-- Records of tblhopdong 01
-- ----------------------------
INSERT INTO `tblhopdong` VALUES (1,700000, 700000,'2020-08-01',1,1,1);

INSERT INTO `tblhopdongdichvu` VALUES (1,1,1,NULL,3500);
INSERT INTO `tblhopdongdichvu` VALUES (2,2,1,NULL,15000);
INSERT INTO `tblhopdongdichvu` VALUES (3,3,1,1,50000);
INSERT INTO `tblhopdongdichvu` VALUES (4,4,1,1,20000);

INSERT INTO `tblhoadon` VALUES (1,1,700000 + 20*3500 + 5*15000 + 1*50000 + 1*20000,'T082020',0,'2020-09-10',4,0,'2020-09-01 09:00:00','2020-09-10 20:00:00');
INSERT INTO `tblhoadondichvu` VALUES (1,1,20,20*3500,1);
INSERT INTO `tblhoadondichvu` VALUES (2,2,5,5*15000,1);
INSERT INTO `tblhoadondichvu` VALUES (3,3,1,1*50000,1);
INSERT INTO `tblhoadondichvu` VALUES (4,4,1,1*20000,1);

INSERT INTO `tblthanhtoan` VALUES (1,915000,'2020-09-10 20:00:00',2);
INSERT INTO `tblthanhtoanhoadon` VALUES (1,915000,1,1);

INSERT INTO `tblhoadon` VALUES (2,1,700000 + 15*3500 + 6*15000 + 1*50000 + 1*20000,'T092020',0,'2020-10-10',4,0,'2020-10-01 09:00:00','2020-10-10 20:00:00');
INSERT INTO `tblhoadondichvu` VALUES (5,1,15,15*3500,2);
INSERT INTO `tblhoadondichvu` VALUES (6,2,6,6*15000,2);
INSERT INTO `tblhoadondichvu` VALUES (7,3,1,1*50000,2);
INSERT INTO `tblhoadondichvu` VALUES (8,4,1,1*20000,2);
INSERT INTO `tblthanhtoan` VALUES (2,912500,'2020-10-10 20:00:00',2);
INSERT INTO `tblthanhtoanhoadon` VALUES (2,912500,2,2);

INSERT INTO `tblhoadon` VALUES (3,1,700000 + 18*3500 + 7*15000 + 1*50000 + 1*20000,'T102020',0,'2020-11-10',2,138000,'2020-11-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblhoadondichvu` VALUES (9,1,18,18*3500,3);
INSERT INTO `tblhoadondichvu` VALUES (10,2,7,7*15000,3);
INSERT INTO `tblhoadondichvu` VALUES (11,3,1,1*50000,3);
INSERT INTO `tblhoadondichvu` VALUES (12,4,1,1*20000,3);
INSERT INTO `tblthanhtoan` VALUES (3,500000,'2020-11-10 20:00:00',2);
INSERT INTO `tblthanhtoanhoadon` VALUES (3,500000,3,3);

INSERT INTO `tblhoadon` VALUES (4,1,700000 + 20*3500 + 3*15000 + 1*50000 + 1*20000,'T112020',438000,'2020-12-10',2,285000,'2020-12-01 09:00:00','2020-12-10 20:00:00');
INSERT INTO `tblhoadondichvu` VALUES (13,1,20,20*3500,4);
INSERT INTO `tblhoadondichvu` VALUES (14,2,3,3*15000,4);
INSERT INTO `tblhoadondichvu` VALUES (15,3,1,1*50000,4);
INSERT INTO `tblhoadondichvu` VALUES (16,4,1,1*20000,4);
INSERT INTO `tblthanhtoan` VALUES (4,500000,'2020-12-10 20:00:00',2);
INSERT INTO `tblthanhtoanhoadon` VALUES (4,500000,4,4);

INSERT INTO `tblhoadon` VALUES (5,1,700000 + 14*3500 + 4*15000 + 1*50000 + 1*20000,'T122020',723000,'2021-01-10',3,0,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblhoadondichvu` VALUES (17,1,14,14*3500,5);
INSERT INTO `tblhoadondichvu` VALUES (18,2,4,4*15000,5);
INSERT INTO `tblhoadondichvu` VALUES (19,3,1,1*50000,5);
INSERT INTO `tblhoadondichvu` VALUES (20,4,1,1*20000,5);
INSERT INTO `tblthanhtoan` VALUES (5,1279000,'2020-12-10 20:00:00',2);
INSERT INTO `tblthanhtoanhoadon` VALUES (5,879000,5,5);
INSERT INTO `tblthanhtoanhoadon` VALUES (6,400000,3,5);

-- ----------------------------
-- Records of tblhopdong 02
-- ----------------------------
INSERT INTO `tblhopdong` VALUES (2,500000, 500000,'2020-07-01',2,1,5);


-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (5,1,2,NULL,3500);
INSERT INTO `tblhopdongdichvu` VALUES (6,2,2,NULL,15000);
INSERT INTO `tblhopdongdichvu` VALUES (7,3,2,1,50000);
INSERT INTO `tblhopdongdichvu` VALUES (8,4,2,2,20000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId ) 
INSERT INTO `tblhoadondichvu` VALUES (21,5,15,15*3500,6);
INSERT INTO `tblhoadondichvu` VALUES (22,6,6,6*15000,6);
INSERT INTO `tblhoadondichvu` VALUES (23,7,1,1*50000,6);
INSERT INTO `tblhoadondichvu` VALUES (24,8,2,2*20000,6);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (6,2,500000 + 15*3500 + 6*15000 + 1*50000 + 2*20000,'T072020',0,'2020-08-10',4,0,'2020-08-01 09:00:00','2020-08-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (6,732500,'2020-08-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (7,732500,6,6);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId ) 
INSERT INTO `tblhoadondichvu` VALUES (25,5,16,16*3500,7);
INSERT INTO `tblhoadondichvu` VALUES (26,6,6,6*15000,7);
INSERT INTO `tblhoadondichvu` VALUES (27,7,1,1*50000,7);
INSERT INTO `tblhoadondichvu` VALUES (28,8,2,2*20000,7);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (7,2,500000 + 16*3500 + 6*15000 + 1*50000 + 2*20000,'T082020',0,'2020-09-10',2,236000,'2020-09-01 09:00:00','2020-11-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (7,400000,'2020-09-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (8,400000,7,7);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId ) 
INSERT INTO `tblhoadondichvu` VALUES (29,5,16,16*3500,8);
INSERT INTO `tblhoadondichvu` VALUES (30,6,6,6*15000,8);
INSERT INTO `tblhoadondichvu` VALUES (31,7,1,1*50000,8);
INSERT INTO `tblhoadondichvu` VALUES (32,8,2,2*20000,8);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (8,2,500000 + 16*3500 + 6*15000 + 1*50000 + 2*20000,'T092020',336000,'2020-10-10',2,336000,'2020-10-01 09:00:00','2020-10-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (8,400000,'2020-10-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (9,400000,8,8);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId ) 
INSERT INTO `tblhoadondichvu` VALUES (33,5,19,19*3500,9);
INSERT INTO `tblhoadondichvu` VALUES (34,6,6,6*15000,9);
INSERT INTO `tblhoadondichvu` VALUES (35,7,1,1*50000,9);
INSERT INTO `tblhoadondichvu` VALUES (36,8,2,2*20000,9);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (9,2,500000 + 19*3500 + 6*15000 + 1*50000 + 2*20000,'T102020',672000,'2020-11-10',3,0,'2020-11-01 09:00:00','2020-11-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (9,846500,'2020-11-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (19,746500,9,9);
INSERT INTO `tblthanhtoanhoadon` VALUES (20,100000,7,9);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId ) 
INSERT INTO `tblhoadondichvu` VALUES (37,5,12,12*3500,10);
INSERT INTO `tblhoadondichvu` VALUES (38,6,9,9*15000,10);
INSERT INTO `tblhoadondichvu` VALUES (39,7,1,1*50000,10);
INSERT INTO `tblhoadondichvu` VALUES (40,8,2,2*20000,10);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (10,2,500000 + 12*3500 + 9*15000 + 1*50000 + 2*20000,'T112020',572000,'2020-12-10',2,100000,'2020-12-01 09:00:00','2020-12-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (10,567000,'2020-12-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (21,567000,10,10);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId ) 
INSERT INTO `tblhoadondichvu` VALUES (41,5,12,12*3500,11);
INSERT INTO `tblhoadondichvu` VALUES (42,6,9,9*15000,11);
INSERT INTO `tblhoadondichvu` VALUES (43,7,1,1*50000,11);
INSERT INTO `tblhoadondichvu` VALUES (44,8,2,2*20000,11);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (11,2,500000 + 12*3500 + 9*15000 + 1*50000 + 2*20000,'T122020',672000,'2020-01-10',2,100000,'2020-01-01 09:00:00','2020-01-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (11,567000,'2020-01-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (22,567000,11,11);


-- ----------------------------
-- Records of tblhopdong 03
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (3,500000, 500000,'2020-11-01',3,1,6);

-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (9,1,3,NULL,3500);
INSERT INTO `tblhopdongdichvu` VALUES (10,2,3,NULL,15000);
INSERT INTO `tblhopdongdichvu` VALUES (11,3,2,1,50000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (45,9,15,15*3500,12);
INSERT INTO `tblhoadondichvu` VALUES (46,10,6,6*15000,12);
INSERT INTO `tblhoadondichvu` VALUES (47,11,1,1*50000,12);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (12,3,500000 + 15*3500 + 6*15000 + 1*50000,'T112020',0,'2020-12-10',2,292500,'2020-12-01 09:00:00','2021-01-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (12,300000,'2020-12-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (23,300000,12,12);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (48,9,10,10*3500,13);
INSERT INTO `tblhoadondichvu` VALUES (49,10,6,6*15000,13);
INSERT INTO `tblhoadondichvu` VALUES (50,11,1,1*50000,13);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (13,3,500000 + 10*3500 + 6*15000 + 1*50000,'T122020',392500,'2021-01-10',2,0,'2021-01-01 09:00:00','2021-01-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (13,775000,'2021-01-10 20:00:00',3);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (24,675000,13,13);
INSERT INTO `tblthanhtoanhoadon` VALUES (25,100000,12,13);

-- ----------------------------
-- Records of tblhopdong 04
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (4,400000, 400000,'2020-12-01',5,1,7);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (12,1,4,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (13,2,4,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (51,12,10,10*3000,14);
INSERT INTO `tblhoadondichvu` VALUES (52,13,6,6*10000,14);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (14,4,400000 + 10*3000 + 6*10000,'T122020',0,'2021-01-10',4,0,'2021-01-01 09:00:00','2021-01-10 20:00:00');
-- tblthanhtoan (id, sotien, ngaythanhtoan, tblsinhvienId)
INSERT INTO `tblthanhtoan` VALUES (14,490000,'2021-01-10 20:00:00',4);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (26,490000,14,14);

-- ----------------------------
-- Records of tblhopdong 05
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (5,400000, 400000,'2020-12-01',6,1,8);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (14,1,5,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (15,2,5,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (53,14,10,10*3000,15);
INSERT INTO `tblhoadondichvu` VALUES (54,15,6,6*10000,15);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (15,5,400000 + 10*3000 + 6*10000,'T122020',0,'2021-01-10',1,490000,'2021-01-01 09:00:00','2021-01-10 20:00:00');


-- ----------------------------
-- Records of tblhopdong 06
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (6,700000, 700000,'2020-11-01',7,1,9);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (16,1,6,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (17,2,6,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (55,16,10,10*3000,16);
INSERT INTO `tblhoadondichvu` VALUES (56,17,6,6*10000,16);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (16,6,700000 + 10*3000 + 6*10000,'T112020',0,'2020-12-01',2,100000,'2020-12-01 09:00:00','2020-12-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (16,690000,'2020-12-10 20:00:00',7);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (27,690000,16,16);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (57,16,10,10*3000,17);
INSERT INTO `tblhoadondichvu` VALUES (58,17,6,6*10000,17);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (17,6,700000 + 10*3000 + 6*10000,'T122020',100000,'2021-01-01',2,200000,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (17,590000,'2021-01-10 20:00:00',7);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (28,590000,17,17);

-- ----------------------------
-- Records of tblhopdong 07
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (7,600000, 600000,'2020-11-01',8,1,11);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (18,1,7,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (19,2,7,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (59,18,10,10*3000,18);
INSERT INTO `tblhoadondichvu` VALUES (60,19,6,6*10000,18);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (18,7,600000 + 10*3000 + 6*10000,'T112020',0,'2020-12-01',4,0,'2020-12-01 09:00:00','2020-12-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (18,690000,'2020-12-10 20:00:00',8);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (29,690000,18,18);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (61,18,10,10*3000,19);
INSERT INTO `tblhoadondichvu` VALUES (62,19,6,6*10000,19);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (19,7,600000 + 10*3000 + 6*10000,'T122020',0,'2021-01-01',2,100000,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (19,590000,'2021-01-10 20:00:00',8);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (30,590000,19,19);

-- ----------------------------
-- Records of tblhopdong 08
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (8,700000, 700000,'2020-11-01',9,1,13);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (20,1,8,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (21,2,8,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (63,20,10,10*3000,20);
INSERT INTO `tblhoadondichvu` VALUES (64,21,6,6*10000,20);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (20,8,700000 + 10*3000 + 6*10000,'T112020',0,'2020-12-01',4,0,'2020-12-01 09:00:00','2020-12-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (20,790000,'2020-12-10 20:00:00',9);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (31,790000,20,20);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (65,20,12,12*3000,21);
INSERT INTO `tblhoadondichvu` VALUES (66,21,6,6*10000,21);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (21,8,700000 + 12*3000 + 6*10000,'T122020',0,'2021-01-01',4,0,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (21,796000,'2021-01-10 20:00:00',9);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (32,796000,21,21);


-- ----------------------------
-- Records of tblhopdong 09
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (9,600000, 600000,'2020-12-01',10,1,15);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (22,1,9,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (23,2,9,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (67,22,15,15*3000,22);
INSERT INTO `tblhoadondichvu` VALUES (68,23,6,6*10000,22);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (22,9,600000 + 15*3000 + 6*10000,'T122020',0,'2021-01-01',2,300000,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (22,405000,'2021-01-10 20:00:00',10);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (33,405000,22,22);



-- ----------------------------
-- Records of tblhopdong 10
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (10,500000, 500000,'2020-12-01',11,1,17);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (24,1,10,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (25,2,10,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (69,24,15,15*3000,23);
INSERT INTO `tblhoadondichvu` VALUES (70,25,6,6*10000,23);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (23,10,500000 + 15*3000 + 6*10000,'T122020',0,'2021-01-01',4,0,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (23,605000,'2021-01-10 20:00:00',11);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (34,605000,23,23);


-- ----------------------------
-- Records of tblhopdong 11
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (11,400000, 400000,'2020-11-01',12,1,19);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (26,1,11,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (27,2,11,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (71,26,10,10*3000,24);
INSERT INTO `tblhoadondichvu` VALUES (72,27,6,6*10000,24);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (24,11,400000 + 10*3000 + 6*10000,'T112020',0,'2020-12-01',2,200000,'2020-12-01 09:00:00','2020-12-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (24,290000,'2020-12-10 20:00:00',12);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (35,290000,24,24);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (73,26,15,15*3000,25);
INSERT INTO `tblhoadondichvu` VALUES (74,27,6,6*10000,25);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (25,11,400000 + 15*3000 + 6*10000,'T122020',200000,'2021-01-01',2,100000,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (25,405000,'2021-01-10 20:00:00',12);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (36,405000,25,25);


-- ----------------------------
-- Records of tblhopdong 12
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (12,500000, 500000,'2020-12-01',13,1,21);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (28,1,12,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (29,2,12,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (75,28,15,15*3000,26);
INSERT INTO `tblhoadondichvu` VALUES (76,29,6,6*10000,26);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (26,12,500000 + 15*3000 + 6*10000,'T122020',0,'2021-01-01',2,505000,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (26,100000,'2021-01-10 20:00:00',13);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (37,100000,26,26);

-- ----------------------------
-- Records of tblhopdong 13
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (13,400000, 400000,'2020-12-01',14,1,23);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (30,1,13,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (31,2,13,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (77,30,15,15*3000,27);
INSERT INTO `tblhoadondichvu` VALUES (78,31,6,6*10000,27);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (27,13,400000 + 15*3000 + 6*10000,'T122020',0,'2021-01-01',2,405000,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (27,100000,'2021-01-10 20:00:00',14);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (38,100000,27,27);

-- ----------------------------
-- Records of tblhopdong 14
-- ----------------------------
-- tblhopdong (id, tiencoc, giathue, ngaybatdau, tblsinhvienkhoaid, tblnhanvienid, tblgiuongid);
INSERT INTO `tblhopdong` VALUES (14,700000, 700000,'2020-12-01',15,1,25);
-- tblhopdongdichvu (id,tblDichvuId, tblHopdongId,soluong,dongia)
INSERT INTO `tblhopdongdichvu` VALUES (32,1,14,NULL,3000);
INSERT INTO `tblhopdongdichvu` VALUES (33,2,14,NULL,10000);

-- tblhoadondichvu ( id, tblHopdongDichvuId, soluongSudung, thanhtien, tblHoadonId )
INSERT INTO `tblhoadondichvu` VALUES (79,32,15,15*3000,28);
INSERT INTO `tblhoadondichvu` VALUES (80,33,6,6*10000,28);
-- tblhoadon (id,tblHopdongId,tienthanghientai, kythanhtoan, tongnocu, ngayphaithanhtoan, trangthai, tiennothanghientai, created_at, updated_at )
INSERT INTO `tblhoadon` VALUES (28,14,700000 + 15*3000 + 6*10000,'T122020',0,'2021-01-01',4,0,'2021-01-01 09:00:00','2021-01-10 20:00:00');
INSERT INTO `tblthanhtoan` VALUES (28,805000,'2021-01-10 20:00:00',15);
-- tblthanhtoanhoadon (id, sotien, tblHoadonId, tblThanhtoanId )
INSERT INTO `tblthanhtoanhoadon` VALUES (39,805000,28,28);

-- ----------------------------
-- Table structure for tblkhoa
-- ----------------------------
DROP TABLE IF EXISTS `tblkhoa`;
CREATE TABLE `tblkhoa`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblTruongId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblkhoa
-- ----------------------------
INSERT INTO `tblkhoa` VALUES (1, 'CNTT', NULL, 1);
INSERT INTO `tblkhoa` VALUES (2, 'MKT', NULL, 1);
INSERT INTO `tblkhoa` VALUES (3, 'Kế toán', NULL, 1);
INSERT INTO `tblkhoa` VALUES (4, 'Quản trị kinh doanh', NULL, 1);
INSERT INTO `tblkhoa` VALUES (5, 'CNTT', NULL, 2);
INSERT INTO `tblkhoa` VALUES (6, 'ATTT', NULL, 2);



-- ----------------------------
-- Table structure for tblnguoi
-- ----------------------------
DROP TABLE IF EXISTS `tblnguoi`;
CREATE TABLE `tblnguoi`  (
  `id` int(11) NOT NULL,
  `hodem` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `socmt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dienthoai` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ghichu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblDiachiId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblnguoi
-- ----------------------------
INSERT INTO `tblnguoi` VALUES (1, 'Nguyễn Văn', 'Vĩ', '1988-09-27','14278890', 'vinv@vega.com.vn', '0343111611', NULL, 1); -- done
INSERT INTO `tblnguoi` VALUES (2, 'Nguyễn Văn', 'Bảo', '1999-01-27','14278123', 'bao@gmail.com', '0987654322', NULL, 29); -- done
INSERT INTO `tblnguoi` VALUES (3, 'Nguyễn Văn', 'Tùng', '1998-01-05','14278234', 'tung@gmail.com', '0987654323', NULL, 28); -- done
INSERT INTO `tblnguoi` VALUES (4, 'Ngô Gia', 'Khánh', '1999-03-05','14278345', 'khanh@gmail.com', '0987654324', NULL, 27); -- done
INSERT INTO `tblnguoi` VALUES (5, 'Nguyễn Sơn', 'Lâm', '1999-01-05','14278456', 'lam@gmail.com', '0987654325', NULL, 26);
INSERT INTO `tblnguoi` VALUES (6, 'Trịnh Văn', 'Nam', '1998-12-01','14278567', 'nam@gmail.com', '0987650321', NULL, 25);
INSERT INTO `tblnguoi` VALUES (7, 'Ngô Văn', 'Chiến', '1999-03-06','14278678', 'chien@gmail.com', '0987654524', NULL, 24);
INSERT INTO `tblnguoi` VALUES (8, 'Nguyễn Thế', 'Bách', '1999-01-05','142712330', 'bach@gmail.com', '0987652325', NULL, 23);
INSERT INTO `tblnguoi` VALUES (9, 'Nguyễn Văn', 'Trường', '1998-12-01','14271234', 'truong@gmail.com', '0987657321', NULL, 22);
INSERT INTO `tblnguoi` VALUES (10, 'Hoàng Thị', 'Anh', '1998-12-02','14272345', 'anh@gmail.com', '0957654321', NULL, 1);
INSERT INTO `tblnguoi` VALUES (11, 'Nguyễn Trọng', 'Bằng', '1999-07-06','14273456', 'bang@gmail.com', '0997654524', NULL, 2);
INSERT INTO `tblnguoi` VALUES (12, 'Nguyễn Tiến', 'Công', '1999-11-05','14274567', 'cong@gmail.com', '0987652525', NULL, 3);
INSERT INTO `tblnguoi` VALUES (13, 'Giáp Mạnh', 'Dũng', '1998-10-01','14275678', 'dung@gmail.com', '0987657721', NULL, 4);
INSERT INTO `tblnguoi` VALUES (14, 'Lê Quang', 'Đạo', '1998-12-21','14276789', 'dao@gmail.com', '0987651321', NULL, 5);
INSERT INTO `tblnguoi` VALUES (15, 'Tạ Khắc', 'Đạt', '1998-10-02','14277899', 'dat@gmail.com', '0977654321', NULL, 6);
INSERT INTO `tblnguoi` VALUES (16, 'Thái Khắc', 'Đường', '1999-06-06','14279999', 'duong@gmail.com', '0983654524', NULL, 7);
INSERT INTO `tblnguoi` VALUES (17, 'Đào Thị', 'Hiên', '1999-12-05','14270000', 'hien@gmail.com', '0987653525', NULL, 8);
INSERT INTO `tblnguoi` VALUES (18, 'Nguyễn Hoàng', 'Hiệp', '1998-10-01','14271111', 'hiep@gmail.com', '0987457721', NULL, 9);
INSERT INTO `tblnguoi` VALUES (19, 'Hà Duy', 'Hoàng', '1998-10-05','14272222', 'hoang@gmail.com', '0877654321', NULL, 10);
INSERT INTO `tblnguoi` VALUES (20, 'Phan Văn', 'Khải', '1999-08-06','14273333', 'khai@gmail.com', '0982654524', NULL, 11);
INSERT INTO `tblnguoi` VALUES (21, 'Lê Duy', 'Khánh', '1999-02-05','14274444', 'khanh@gmail.com', '0987656525', NULL, 13);
INSERT INTO `tblnguoi` VALUES (22, 'Phạm Văn', 'Khoa', '1998-10-01','14275555', 'khoa@gmail.com', '0987357721', NULL, 12);
INSERT INTO `tblnguoi` VALUES (23, 'Phạm Tùng', 'Lâm', '1998-10-05','14276666', 'lam@gmail.com', '0877154321', NULL, 15);
INSERT INTO `tblnguoi` VALUES (24, 'Nguyễn Quang', 'Linh', '1998-08-06','14277777', 'linh@gmail.com', '0981154524', NULL, 14);
INSERT INTO `tblnguoi` VALUES (25, 'Trần Khắc', 'Nan', '1999-02-05','14278888', 'nan@gmail.com', '0987156525', NULL, 16);
INSERT INTO `tblnguoi` VALUES (26, 'Lê Trọng', 'Nghia', '1999-10-01','14279999', 'khoa@gmail.com', '0986357721', NULL, 17);
INSERT INTO `tblnguoi` VALUES (27, 'Trương Thanh', 'Phong', '1998-10-11','14272233', 'phong@gmail.com', '0817154321', NULL, 18);
INSERT INTO `tblnguoi` VALUES (28, 'Nguyễn Hoàng', 'Phuc', '1998-08-06','14273344', 'phuc@gmail.com', '0981254524', NULL, 19);
INSERT INTO `tblnguoi` VALUES (29, 'Bùi Quang', 'Sơn', '1999-02-05','14274455', 'son@gmail.com', '0988565251', NULL, 20);
INSERT INTO `tblnguoi` VALUES (30, 'Nguyễn Thị Thanh', 'Tâm', '1999-10-01','14275566', 'tam@gmail.com', '0987377721', NULL, 30);
INSERT INTO `tblnguoi` VALUES (31, 'Nguyễn Văn', 'An', '1998-01-05','14276677', 'an@gmail.com', '0987654321', NULL, 21); -- done

-- ----------------------------
-- Table structure for tblnhanvien
-- ----------------------------
DROP TABLE IF EXISTS `tblnhanvien`;
CREATE TABLE `tblnhanvien`  (
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tblThanhvienId` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblnhanvien
-- ----------------------------
INSERT INTO `tblnhanvien` VALUES ('vinguyen', '123456', 1);

-- ----------------------------
-- Table structure for tblphong
-- ----------------------------
DROP TABLE IF EXISTS `tblphong`;
CREATE TABLE `tblphong`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `succhua` tinyint(3) NOT NULL,
  `coDieuhoa` tinyint(3) NOT NULL,
  `ghichu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblTohopId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblphong
-- ----------------------------
INSERT INTO `tblphong` VALUES (1,'101',4,1,NULL,1);
INSERT INTO `tblphong` VALUES (2,'102',4,0,NULL,1);
INSERT INTO `tblphong` VALUES (3,'103',8,1,NULL,1);
INSERT INTO `tblphong` VALUES (4,'104',8,0,NULL,1);
INSERT INTO `tblphong` VALUES (5,'201',4,1,NULL,1);
INSERT INTO `tblphong` VALUES (6,'202',4,0,NULL,1);
INSERT INTO `tblphong` VALUES (7,'203',8,1,NULL,1);
INSERT INTO `tblphong` VALUES (8,'204',8,0,NULL,1);


-- ----------------------------
-- Table structure for tblgiuong
-- ----------------------------
DROP TABLE IF EXISTS `tblgiuong`;
CREATE TABLE `tblgiuong`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vitri` tinyint(255) NOT NULL COMMENT '1: Trên, 0: Dưới',
  `giathue` float(255, 0) NULL DEFAULT NULL,
  `tblPhongId` int(11) NOT NULL,
  `trangthai` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblgiuong
-- ----------------------------
INSERT INTO `tblgiuong` VALUES (1,'P101G01',1,700000,1,1); -- done
INSERT INTO `tblgiuong` VALUES (2,'P101G02',1,700000,1,0);
INSERT INTO `tblgiuong` VALUES (3,'P101G03',0,600000,1,0);
INSERT INTO `tblgiuong` VALUES (4,'P101G04',0,600000,1,0);
INSERT INTO `tblgiuong` VALUES (5,'P102G01',1,500000,2,1); -- done
INSERT INTO `tblgiuong` VALUES (6,'P102G02',1,500000,2,1); -- done
INSERT INTO `tblgiuong` VALUES (7,'P102G03',0,400000,2,1); -- done
INSERT INTO `tblgiuong` VALUES (8,'P102G04',0,400000,2,1); -- done
INSERT INTO `tblgiuong` VALUES (9,'P103G01',1,700000,3,1); -- done
INSERT INTO `tblgiuong` VALUES (10,'P103G02',1,700000,3,0);
INSERT INTO `tblgiuong` VALUES (11,'P103G03',0,600000,3,1); -- done
INSERT INTO `tblgiuong` VALUES (12,'P103G04',0,600000,3,0);
INSERT INTO `tblgiuong` VALUES (13,'P103G05',1,700000,3,1); -- done
INSERT INTO `tblgiuong` VALUES (14,'P103G06',1,700000,3,0);
INSERT INTO `tblgiuong` VALUES (15,'P103G07',0,600000,3,1); -- done
INSERT INTO `tblgiuong` VALUES (16,'P103G08',0,600000,3,0);
INSERT INTO `tblgiuong` VALUES (17,'P104G01',1,500000,4,1); -- done
INSERT INTO `tblgiuong` VALUES (18,'P104G02',1,500000,4,0);
INSERT INTO `tblgiuong` VALUES (19,'P104G03',0,400000,4,1); -- done
INSERT INTO `tblgiuong` VALUES (20,'P104G04',0,400000,4,0);
INSERT INTO `tblgiuong` VALUES (21,'P104G05',1,500000,4,1); -- done
INSERT INTO `tblgiuong` VALUES (22,'P104G06',1,500000,4,0);
INSERT INTO `tblgiuong` VALUES (23,'P104G07',0,400000,4,1); -- done
INSERT INTO `tblgiuong` VALUES (24,'P104G08',0,400000,4,0);
INSERT INTO `tblgiuong` VALUES (25,'P201G01',1,700000,5,1); -- done
INSERT INTO `tblgiuong` VALUES (26,'P201G02',1,700000,5,0);
INSERT INTO `tblgiuong` VALUES (27,'P201G03',0,600000,5,1);
INSERT INTO `tblgiuong` VALUES (28,'P201G04',0,600000,5,0);
INSERT INTO `tblgiuong` VALUES (29,'P202G01',1,500000,6,1);
INSERT INTO `tblgiuong` VALUES (30,'P202G02',1,500000,6,0);
INSERT INTO `tblgiuong` VALUES (31,'P202G03',0,400000,6,1);
INSERT INTO `tblgiuong` VALUES (32,'P202G04',0,400000,6,0);
INSERT INTO `tblgiuong` VALUES (33,'P203G01',1,700000,7,1);
INSERT INTO `tblgiuong` VALUES (34,'P203G02',1,700000,7,0);
INSERT INTO `tblgiuong` VALUES (35,'P203G03',0,600000,7,1);
INSERT INTO `tblgiuong` VALUES (36,'P203G04',0,600000,7,0);
INSERT INTO `tblgiuong` VALUES (37,'P203G05',1,700000,7,1);
INSERT INTO `tblgiuong` VALUES (38,'P203G06',1,700000,7,0);
INSERT INTO `tblgiuong` VALUES (39,'P203G07',0,600000,7,1);
INSERT INTO `tblgiuong` VALUES (40,'P203G08',0,600000,7,1);
INSERT INTO `tblgiuong` VALUES (41,'P204G01',1,500000,8,1);
INSERT INTO `tblgiuong` VALUES (42,'P204G02',1,500000,8,1);
INSERT INTO `tblgiuong` VALUES (43,'P204G03',0,400000,8,1);
INSERT INTO `tblgiuong` VALUES (44,'P204G04',0,400000,8,1);
INSERT INTO `tblgiuong` VALUES (45,'P204G05',1,500000,8,1);
INSERT INTO `tblgiuong` VALUES (46,'P204G06',1,500000,8,1);
INSERT INTO `tblgiuong` VALUES (47,'P204G07',0,400000,8,1);
INSERT INTO `tblgiuong` VALUES (48,'P204G08',0,400000,8,1);


-- ----------------------------
-- Table structure for tblsinhvien
-- ----------------------------
DROP TABLE IF EXISTS `tblsinhvien`;
CREATE TABLE `tblsinhvien`  (
  `masv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tblNguoiId` int(11) NOT NULL,
  PRIMARY KEY (`tblNguoiId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblsinhvien
-- ----------------------------
INSERT INTO `tblsinhvien` VALUES ('B16DCCN003', 2); -- done
INSERT INTO `tblsinhvien` VALUES ('B16DCCN005', 3); -- done
INSERT INTO `tblsinhvien` VALUES ('B16DCCN007', 4); -- done
INSERT INTO `tblsinhvien` VALUES ('B16DCCN008', 5);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN009', 6);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN010', 7); -- done
INSERT INTO `tblsinhvien` VALUES ('B16DCCN101', 8); -- done
INSERT INTO `tblsinhvien` VALUES ('B16DCCN201', 9);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN301', 10);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN011', 11);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN021', 12);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN031', 13);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN041', 14);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN051', 15);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN061', 16);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN071', 17);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN081', 18);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN091', 19);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN201', 20);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN202', 21);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN021', 22);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN031', 23);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN041', 24);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN051', 25);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN061', 26);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN071', 27);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN081', 28);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN091', 29);
INSERT INTO `tblsinhvien` VALUES ('B17DCCN111', 30);
INSERT INTO `tblsinhvien` VALUES ('B16DCCN001', 31); -- done



-- ----------------------------
-- Table structure for tblsinhvienkhoa
-- ----------------------------
DROP TABLE IF EXISTS `tblsinhvienkhoa`;
CREATE TABLE `tblsinhvienkhoa`  (
  `id` int(11) NOT NULL,
  `nienkhoa` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `danghoc` tinyint(3) NOT NULL,
  `tblSinhvienId` int(11) NOT NULL,
  `tblKhoaId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tblsinhvienkhoa
-- ----------------------------
INSERT INTO `tblsinhvienkhoa` VALUES (2,'2016-2021', 1,2,1); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (3,'2016-2021', 1,3,1); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (4,'2016-2021', 1,4,1); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (5,'2016-2021', 1,5,1); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (6,'2016-2021', 1,6,2); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (7,'2016-2021', 1,7,2); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (8,'2016-2021', 1,8,3); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (9,'2016-2021', 1,9,3); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (10,'2016-2021', 1,10,4); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (11,'2016-2021', 1,11,4); -- done
INSERT INTO `tblsinhvienkhoa` VALUES (12,'2016-2021', 1,12,4); --
INSERT INTO `tblsinhvienkhoa` VALUES (13,'2016-2021', 1,13,4);
INSERT INTO `tblsinhvienkhoa` VALUES (14,'2016-2021', 1,14,4);
INSERT INTO `tblsinhvienkhoa` VALUES (15,'2017-2027', 1,15,1);
INSERT INTO `tblsinhvienkhoa` VALUES (16,'2017-2022', 1,16,2);
INSERT INTO `tblsinhvienkhoa` VALUES (17,'2017-2022', 1,17,2);
INSERT INTO `tblsinhvienkhoa` VALUES (18,'2017-2022', 1,18,3);
INSERT INTO `tblsinhvienkhoa` VALUES (19,'2017-2022', 1,19,4);
INSERT INTO `tblsinhvienkhoa` VALUES (20,'2017-2022', 1,20,5);
INSERT INTO `tblsinhvienkhoa` VALUES (21,'2017-2022', 1,21,6);
INSERT INTO `tblsinhvienkhoa` VALUES (22,'2017-2022', 1,22,3);
INSERT INTO `tblsinhvienkhoa` VALUES (23,'2017-2022', 1,23,3);
INSERT INTO `tblsinhvienkhoa` VALUES (24,'2017-2022', 1,24,6);
INSERT INTO `tblsinhvienkhoa` VALUES (25,'2017-2022', 1,25,1);
INSERT INTO `tblsinhvienkhoa` VALUES (26,'2017-2022', 1,26,2);
INSERT INTO `tblsinhvienkhoa` VALUES (27,'2017-2022', 1,27,2);
INSERT INTO `tblsinhvienkhoa` VALUES (28,'2017-2022', 1,28,3);
INSERT INTO `tblsinhvienkhoa` VALUES (29,'2017-2022', 1,29,4);
INSERT INTO `tblsinhvienkhoa` VALUES (30,'2017-2022', 1,30,5);
INSERT INTO `tblsinhvienkhoa` VALUES (1,'2016-2021', 1,31,1);

-- ----------------------------
-- Table structure for tbltohop
-- ----------------------------
DROP TABLE IF EXISTS `tbltohop`;
CREATE TABLE `tbltohop`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblDiachiId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tbltohop
-- ----------------------------
INSERT INTO `tbltohop` VALUES (1,'KTX Hà Đông', NULL,3);


-- ----------------------------
-- Table structure for tbltruong
-- ----------------------------
DROP TABLE IF EXISTS `tbltruong`;
CREATE TABLE `tbltruong`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblDiachiId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;
-- ----------------------------
-- Records of tbltruong
-- ----------------------------
INSERT INTO `tbltruong` VALUES (1,'PTIT', NULL,3);
INSERT INTO `tbltruong` VALUES (2,'ĐH Kiến trúc', NULL,5);



SET FOREIGN_KEY_CHECKS = 1;
