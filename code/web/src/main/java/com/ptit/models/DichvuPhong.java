package com.ptit.models;

public class DichvuPhong {

    private int id;
    private int sodau, socuoi, soluongPhongSudung;
    private Phong phong;
    private Dichvu dichvu;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSodau() {
        return sodau;
    }

    public void setSodau(int sodau) {
        this.sodau = sodau;
    }

    public int getSocuoi() {
        return socuoi;
    }

    public void setSocuoi(int socuoi) {
        this.socuoi = socuoi;
    }

    public int getSoluongPhongSudung() {
        return soluongPhongSudung;
    }

    public void setSoluongPhongSudung(int soluongPhongSudung) {
        this.soluongPhongSudung = soluongPhongSudung;
    }

    public Phong getPhong() {
        return phong;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }

    public Dichvu getDichvu() {
        return dichvu;
    }

    public void setDichvu(Dichvu dichvu) {
        this.dichvu = dichvu;
    }
}
