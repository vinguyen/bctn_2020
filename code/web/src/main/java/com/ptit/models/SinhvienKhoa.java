package com.ptit.models;

public class SinhvienKhoa {

    private int id;
    private String nienkhoa;
    private Sinhvien sinhvien;
    private Khoa khoa;
    private boolean danghoc;

    public SinhvienKhoa() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNienkhoa() {
        return nienkhoa;
    }

    public void setNienkhoa(String nienkhoa) {
        this.nienkhoa = nienkhoa;
    }

    public Sinhvien getSinhvien() {
        return sinhvien;
    }

    public void setSinhvien(Sinhvien sinhvien) {
        this.sinhvien = sinhvien;
    }

    public Khoa getKhoa() {
        return khoa;
    }

    public void setKhoa(Khoa khoa) {
        this.khoa = khoa;
    }

    public boolean isDanghoc() {
        return danghoc;
    }

    public void setDanghoc(boolean danghoc) {
        this.danghoc = danghoc;
    }
}
