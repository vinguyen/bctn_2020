package com.ptit.models;

import java.util.Date;

public class ThanhToan {

    private int id;
    private float sotien;
    private Date ngaythanhtoan;
    private ThanhToan dsThanhtoan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getSotien() {
        return sotien;
    }

    public void setSotien(float sotien) {
        this.sotien = sotien;
    }

    public Date getNgaythanhtoan() {
        return ngaythanhtoan;
    }

    public void setNgaythanhtoan(Date ngaythanhtoan) {
        this.ngaythanhtoan = ngaythanhtoan;
    }
}
