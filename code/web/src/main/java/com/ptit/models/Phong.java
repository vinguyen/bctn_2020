package com.ptit.models;

public class Phong {

    private int id;
    private String ten;
    private int succhua;
    private boolean coDieuhoa;
    private String ghichu;
    private Tohop tohop;

    public Phong() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getSucchua() {
        return succhua;
    }

    public void setSucchua(int succhua) {
        this.succhua = succhua;
    }

    public boolean isCoDieuhoa() {
        return coDieuhoa;
    }

    public void setCoDieuhoa(boolean coDieuhoa) {
        this.coDieuhoa = coDieuhoa;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public Tohop getTohop() {
        return tohop;
    }

    public void setTohop(Tohop tohop) {
        this.tohop = tohop;
    }
}
