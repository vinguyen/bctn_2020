package com.ptit.models;

import java.util.ArrayList;
import java.util.Date;

public class Hoadon {

    private int id;
    private Hopdong hopdong;
    private float tienthangHientai;
    private String kyThanhtoan;
    private float tongnocu;
    private float tongnoThangHientai;
    private Date ngayphaiThanhtoan;

    public Date getNgayphaiThanhtoan() {
        return ngayphaiThanhtoan;
    }

    public void setNgayphaiThanhtoan(Date ngayphaiThanhtoan) {
        this.ngayphaiThanhtoan = ngayphaiThanhtoan;
    }

    private ArrayList<HoadonDichvu> dsHoadonDichvu;
    private ArrayList<ThanhtoanHoadon> dsThanhtoanHoadon;

    public String getKyThanhtoan() {
        return kyThanhtoan;
    }

    public void setKyThanhtoan(String kyThanhtoan) {
        this.kyThanhtoan = kyThanhtoan;
    }

    public float getTongnoThangHientai() {
        return tongnoThangHientai;
    }

    public void setTongnoThangHientai(float tongnoThangHientai) {
        this.tongnoThangHientai = tongnoThangHientai;
    }

    public ArrayList<ThanhtoanHoadon> getDsThanhtoanHoadon() {
        return dsThanhtoanHoadon;
    }

    public void setDsThanhtoanHoadon(ArrayList<ThanhtoanHoadon> dsThanhtoanHoadon) {
        this.dsThanhtoanHoadon = dsThanhtoanHoadon;
    }

    private int trangthai;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Hopdong getHopdong() {
        return hopdong;
    }

    public void setHopdong(Hopdong hopdong) {
        this.hopdong = hopdong;
    }

    public float getTienthangHientai() {
        return tienthangHientai;
    }

    public void setTienthangHientai(float tienthangHientai) {
        this.tienthangHientai = tienthangHientai;
    }

    public float getTongnocu() {
        return tongnocu;
    }

    public void setTongnocu(float tongnocu) {
        this.tongnocu = tongnocu;
    }

    public ArrayList<HoadonDichvu> getDsHoadonDichvu() {
        return dsHoadonDichvu;
    }

    public void setDsHoadonDichvu(ArrayList<HoadonDichvu> dsHoadonDichvu) {
        this.dsHoadonDichvu = dsHoadonDichvu;
    }

    public int getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(int trangthai) {
        this.trangthai = trangthai;
    }
}
