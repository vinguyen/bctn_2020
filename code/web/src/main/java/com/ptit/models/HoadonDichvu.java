package com.ptit.models;

public class HoadonDichvu extends HopdongDichvu {

    private int soluongSudung;
    private float thanhtien;

    public HoadonDichvu() {

    }

    public int getSoluongSudung() {
        return soluongSudung;
    }

    public void setSoluongSudung(int soluongSudung) {
        this.soluongSudung = soluongSudung;
    }

    public float getThanhtien() {
        return thanhtien;
    }

    public void setThanhtien(float thanhtien) {
        this.thanhtien = thanhtien;
    }
}
