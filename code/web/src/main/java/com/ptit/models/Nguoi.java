package com.ptit.models;

public class Nguoi {

    private int id;
    private String hodem,ten,ngaysinh,socmt, email, sodt, ghichu;
    private Diachi diachi;
    private Hoten hoten;

    public String getSocmt() {
        return socmt;
    }

    public void setSocmt(String socmt) {
        this.socmt = socmt;
    }

    public Hoten getHoten() {
        return hoten;
    }

    public void setHoten(Hoten hoten) {
        this.hoten = hoten;
    }

    public Nguoi() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHodem() {
        return hodem;
    }

    public void setHodem(String hodem) {
        this.hodem = hodem;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSodt() {
        return sodt;
    }

    public void setSodt(String sodt) {
        this.sodt = sodt;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public Diachi getDiachi() {
        return diachi;
    }

    public void setDiachi(Diachi diachi) {
        this.diachi = diachi;
    }

}
