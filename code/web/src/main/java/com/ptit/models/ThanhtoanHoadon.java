package com.ptit.models;

public class ThanhtoanHoadon {

    private float sotienThanhtoan;
    private ThanhToan thanhToan;

    public ThanhToan getThanhToan() {
        return thanhToan;
    }

    public void setThanhToan(ThanhToan thanhToan) {
        this.thanhToan = thanhToan;
    }

    public float getSotienThanhtoan() {
        return sotienThanhtoan;
    }

    public void setSotienThanhtoan(float sotienThanhtoan) {
        this.sotienThanhtoan = sotienThanhtoan;
    }
}
