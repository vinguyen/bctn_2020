package com.ptit.models;

public class Giuong {

    private int id;
    private String ten;
    private boolean vitri;
    private float giathue;
    private Phong phong;

    public Giuong() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public boolean isVitri() {
        return vitri;
    }

    public void setVitri(boolean vitri) {
        this.vitri = vitri;
    }

    public float getGiathue() {
        return giathue;
    }

    public void setGiathue(float giathue) {
        this.giathue = giathue;
    }

    public Phong getPhong() {
        return phong;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }
}
