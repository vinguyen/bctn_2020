package com.ptit.models;

public class Tohop {

    private int id;
    private String ten, mota;
    private Diachi diachi;

    public Tohop(String ten, String mota) {
        this.ten = ten;
        this.mota = mota;
    }

    public Tohop(String ten, String mota, Diachi diachi) {
        this.ten = ten;
        this.mota = mota;
        this.diachi = diachi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public Diachi getDiachi() {
        return diachi;
    }

    public void setDiachi(Diachi diachi) {
        this.diachi = diachi;
    }
}
