package com.ptit.models;

public class HopdongDichvu {

    private int id;
    private Dichvu dichvu;
    private int soluong;
    private float dongia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Dichvu getDichvu() {
        return dichvu;
    }

    public void setDichvu(Dichvu dichvu) {
        this.dichvu = dichvu;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public float getDongia() {
        return dongia;
    }

    public void setDongia(float dongia) {
        this.dongia = dongia;
    }
}
