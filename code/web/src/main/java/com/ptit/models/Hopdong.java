package com.ptit.models;

import java.util.Date;

public class Hopdong extends SinhvienKhoa {

    private int id;
    private Giuong giuong;
    private Nhanvien nhanvien;
    private float tiencoc;
    private float tienthue;
    private Date ngaybatdau;
    private HopdongDichvu[] dsHopdongDichvu;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Giuong getGiuong() {
        return giuong;
    }

    public void setGiuong(Giuong giuong) {
        this.giuong = giuong;
    }

    public Nhanvien getNhanvien() {
        return nhanvien;
    }

    public void setNhanvien(Nhanvien nhanvien) {
        this.nhanvien = nhanvien;
    }

    public float getTiencoc() {
        return tiencoc;
    }

    public void setTiencoc(float tiencoc) {
        this.tiencoc = tiencoc;
    }

    public float getTienthue() {
        return tienthue;
    }

    public void setTienthue(float tienthue) {
        this.tienthue = tienthue;
    }

    public Date getNgaybatdau() {
        return ngaybatdau;
    }

    public void setNgaybatdau(Date ngaybatdau) {
        this.ngaybatdau = ngaybatdau;
    }

    public HopdongDichvu[] getDsHopdongDichvu() {
        return dsHopdongDichvu;
    }

    public void setDsHopdongDichvu(HopdongDichvu[] dsHopdongDichvu) {
        this.dsHopdongDichvu = dsHopdongDichvu;
    }
}
