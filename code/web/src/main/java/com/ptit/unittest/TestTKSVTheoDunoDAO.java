package com.ptit.unittest;

import com.ptit.dao.DSKhoannoSVDAO;
import com.ptit.dao.TKSVTheoDunoDAO;
import com.ptit.models.DSKhoannoSV;
import com.ptit.models.TKSVTheoDuno;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class TestTKSVTheoDunoDAO {
    TKSVTheoDunoDAO tksvDAO = new TKSVTheoDunoDAO();

    @Test
    public void getTKSVTheoDunoDAO_testChuan01() {
        // Đang tồn tại sinh viên có dư nợ
        ArrayList<TKSVTheoDuno> tksv = tksvDAO.getTKSVTheoDuno();
        Assert.assertNotNull(tksv);
    }

    @Test
    public void getTKSVTheoDunoDao_testChuan02() {
        // Đang tồn tại sinh viên có dư nợ
        // Sinh viên đần tiên có tổng nợ lớn hơn hoặc bằng sinh viên ở hàng thứ 2

        ArrayList<TKSVTheoDuno> tksv = tksvDAO.getTKSVTheoDuno();
        boolean check = true;
        if (tksv.size() > 1) {
            for (int i = 0; i < tksv.size() - 1; i++) {
                if (tksv.get(i).getTongtienno() < tksv.get(i + 1).getTongtienno()) {
                    check = false;
                    break;
                }
            }
        }
        Assert.assertNotNull(tksv);
        Assert.assertTrue(check);
    }

    @Test
    public void getTKSVTheoDunoDAO_testChuan03() {
        // Sinh viên đã thanh toán đủ có mã sv B16DCCN008
        // Sinh viên đã thanh toán đủ không xuất hiện trong bảng

        String masv = "B16DCCN008";
        ArrayList<TKSVTheoDuno> tksv = tksvDAO.getTKSVTheoDuno();
        boolean check = true;
        for (TKSVTheoDuno sv : tksv) {
            if (sv.getSinhvien().getMasv().equals(masv)) {
                check = false;
                break;
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getTKSVTheoDunoDAO_testChuan04() {
        // Sinh viên xuất hiện trong kết quả thống kê đều có tổng nợ lớn hơn 0;
        ArrayList<TKSVTheoDuno> tksv = tksvDAO.getTKSVTheoDuno();
        boolean check = true;
        for (TKSVTheoDuno sv : tksv) {
            if (sv.getTongtienno() <= 0) {
                check = false;
                break;
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getSVTheoDunoDAO_testChuan05() {
        // Tổng khoản nợ của sinh viên phải chuẩn
        // Danh sách khoản nợ của sinh viên không được rỗng
        ArrayList<TKSVTheoDuno> tksv = tksvDAO.getTKSVTheoDuno();

        if (tksv.size() > 0) {
            String masv = tksv.get(0).getSinhvien().getMasv();
            DSKhoannoSVDAO dsKhoannoSVDAO = new DSKhoannoSVDAO();
            ArrayList<DSKhoannoSV> dsKhoannoSVS = dsKhoannoSVDAO.getDSKhoannoSV(masv);

            Assert.assertNotNull(dsKhoannoSVS);

            float tong = 0;
            for (DSKhoannoSV khoannoSV : dsKhoannoSVS) {
                tong+= khoannoSV.getTongnoThangHientai();
            }

            Assert.assertEquals(tong, tksv.get(0).getTongtienno(), 0.0);

        }

    }

    @Test
    public void getSVTheoDunoDAO_testChuan06() {
        // MSV B16DCCN003 đang có dư nợ.
        String msv = "B16DCCN003";
        ArrayList<TKSVTheoDuno> tk = tksvDAO.getTKSVTheoDuno();
        boolean check = false;
        for (TKSVTheoDuno tksvTheoDuno : tk) {
            if (tksvTheoDuno.getSinhvien().getMasv().equals(msv)) {
                check = true;
                break;
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getSVTheoDunoDAO_testChuan07() {
        // Hiển thị đúng thông tin của sinh viên.
        String msv = "B16DCCN003";
        String ten = "Bảo";
        String socmt = "14278123";
        String sodt = "0987654322";
        String tenTruong = "PTIT";
        String tenKhoa = "CNTT";
        String nienkhoa = "2016-2021";

        ArrayList<TKSVTheoDuno> tk = tksvDAO.getTKSVTheoDuno();
        for (TKSVTheoDuno sv : tk) {
            if (sv.getSinhvien().getMasv().equals(msv)) {
                Assert.assertEquals(ten, sv.getSinhvien().getHoten().getTen());
                Assert.assertEquals(socmt,sv.getSinhvien().getSocmt());
                Assert.assertEquals(sodt, sv.getSinhvien().getSodt());
                Assert.assertEquals(tenTruong, sv.getKhoa().getTruong().getTen());
                Assert.assertEquals(tenKhoa, sv.getKhoa().getTen());
                Assert.assertEquals(nienkhoa, sv.getNienkhoa());
            }
        }
    }

    @Test
    public void getSVTheoDunoDAO_testChuan08() {
        // Hiển thị đúng thông tin phòng của sinh viên.
        String msv = "B16DCCN003";
        String tenphong = "102";
        int soGiuong = 4;
        String tengiuong = "P102G01";

        ArrayList<TKSVTheoDuno> tk = tksvDAO.getTKSVTheoDuno();
        for (TKSVTheoDuno sv : tk) {
            if (sv.getSinhvien().getMasv().equals(msv)) {
                Assert.assertEquals(tengiuong, sv.getGiuong().getTen());
                Assert.assertEquals(tenphong, sv.getGiuong().getPhong().getTen());
                Assert.assertEquals(soGiuong, sv.getGiuong().getPhong().getSucchua());
                Assert.assertTrue(sv.getGiuong().isVitri()); // Tầng trên
                Assert.assertFalse(sv.getGiuong().getPhong().isCoDieuhoa()); // Không điều hòa
            }
        }
    }

    @Test
    public void getSVTheoDunoDAO_testChuan09() {
        // Hiển thị đúng số tiền nợ của sinh viên
        String msv = "B16DCCN003";
        float sotienno = (float) 872000.0;

        ArrayList<TKSVTheoDuno> tk = tksvDAO.getTKSVTheoDuno();
        for (TKSVTheoDuno sv : tk) {
            if (sv.getSinhvien().getMasv().equals(msv)) {
                Assert.assertEquals(sotienno, sv.getTongtienno(), 0.0);
            }
        }
    }

    @Test
    public void getSVTheoDunoDAO_testChuan10() {
        // Tính đúng tổng số tiền nợ của các sinh viên.
    }

}
