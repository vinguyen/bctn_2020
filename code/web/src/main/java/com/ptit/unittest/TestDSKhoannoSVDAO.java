package com.ptit.unittest;

import com.ptit.dao.DSKhoannoSVDAO;
import com.ptit.models.DSKhoannoSV;
import com.ptit.models.HoadonDichvu;
import com.ptit.models.ThanhtoanHoadon;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class TestDSKhoannoSVDAO {
    DSKhoannoSVDAO dsknDAO = new DSKhoannoSVDAO();

    @Test
    public void getDSKhoannoSV_chuan01() {
        // Sinh viên có 4 khoản nợ
        String masv = "B16DCCN003";
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        Assert.assertNotNull(knsv);
        Assert.assertEquals(4,knsv.size());
    }

    @Test
    public void getDSKhoannoSV_chuan02() {
        // Sinh viên có 1 khoản nợ
        String masv = "B16DCCN005";
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        Assert.assertNotNull(knsv);
        Assert.assertEquals(1,knsv.size());
    }

    @Test
    public void getDSKhoannoSV_chuan03() {
        // Tổng tiền nợ của 1 khoản nợ lớn hơn 0
        String masv = "B16DCCN003";
        boolean check = true;
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        for (DSKhoannoSV kn : knsv) {
            if (kn.getTongnoThangHientai() <= 0) {
                check = false;
                break;
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getDSKhoanoSV_chuan04() {
        // Danh sách các khoản nợ hiện đúng thứ tự tăng dần của ngày phải thanh toán.
        String masv = "B16DCCN003";
        boolean check = true;
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);

        if (knsv.size() > 1) {
            for (int i = 0; i < knsv.size() - 1; i++) {
                if (knsv.get(i).getNgayphaiThanhtoan().compareTo(knsv.get(i + 1).getNgayphaiThanhtoan())>0) {
                    check = false;
                    break;
                }
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getDSKhoannoSV_chuan05() {
        // Khoản nợ đã thanh toán hết không có trong danh sách
        String masv = "B16DCCN003";
        String kythanhtoan = "T072020";

        boolean check = true;
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        for (DSKhoannoSV ds : knsv) {
            if (ds.getKyThanhtoan().equals(kythanhtoan)) {
                check = false;
                break;
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getDSKhoannoSV_chuan06() {
        // Sinh viên không có khoản nợ nào
        String masv = "B16DCCN008";
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        Assert.assertNull(knsv);
    }

    @Test
    public void getDSKhoannoSV_chuan07() {
        // Hiển thị đúng số tiền nợ của khoản nợ.
        String masv = "B16DCCN001";
        String kythanhtoan = "T102020";
        float sotienno = (float) 138000.00;
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        for (DSKhoannoSV ds : knsv) {
            if (ds.getKyThanhtoan().equals(kythanhtoan)) {
                Assert.assertEquals(sotienno, ds.getTongnoThangHientai(),0.0);
            }
        }
    }

    @Test
    public void getDSKhoannoSV_chuan08() {
        // Hiển thị đúng tổng số tiền nợ của sinh viên.
        String masv = "B16DCCN001";
        float tongtieno = (float)423000.00;
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        float tongtien = 0;
        for (DSKhoannoSV ds : knsv) {
            tongtien += ds.getTongnoThangHientai();
        }
        Assert.assertEquals(tongtien, tongtieno, 0.0);
    }

    @Test
    public void getDSKhoannoSV_chuan09() {
        // Khoản nợ chưa thanh toán hết phải hiển thị trong danh sách.
        String masv = "B16DCCN001";
        String kythanhtoan = "T102020";
        ArrayList<DSKhoannoSV> knsv = dsknDAO.getDSKhoannoSV(masv);
        boolean check = false;
        for (DSKhoannoSV ds : knsv) {
            if (ds.getKyThanhtoan().equals(kythanhtoan)) {
                check = true;
                break;
            }
        }
        Assert.assertTrue(check);
    }

    @Test
    public void getHoadonNoSV_chuan10() {
        // Hóa đơn không tồn tại
        int mahoadon = 10000;
        DSKhoannoSV ds = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNull(ds);
    }

    @Test
    public void getHoadonNoSV_chuan11() {
        // Hóa đơn tồn tại
        int mahoadon = 7;
        DSKhoannoSV ds = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(ds);
    }

    @Test
    public void getHoadonNoSV_chuan12() {
        // Tổng tiền phòng = tiền phòng + tiền dịch vụ.
        int mahoadon = 7;
        DSKhoannoSV ds = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(ds);
        float tiendichvu = 0;
        for (HoadonDichvu hdsv : ds.getDsHoadonDichvu()) {
            tiendichvu += hdsv.getThanhtien();
        }
        Assert.assertEquals(ds.getTienthangHientai(),tiendichvu+ds.getHopdong().getTienthue(),0.0);
    }

    @Test
    public void getHoadonNoSV_chuan13() {
        // Check tiền nợ so với giao diện ds khoản nợ
        String masv = "B16DCCN003";
        ArrayList<DSKhoannoSV> ds = dsknDAO.getDSKhoannoSV(masv);
        for (DSKhoannoSV hd : ds) {
            DSKhoannoSV kn = dsknDAO.getHoadonNoSV(hd.getId());
            Assert.assertEquals(hd.getTongnoThangHientai(),kn.getTongnoThangHientai(), 0.0);
        }
    }

    @Test
    public void getHoadonNoSV_chuan14() {
        // Check tinh tổng tiền dịch vụ
        float tiendichvu = (float) 236000.0;
        int mahoadon = 7;
        DSKhoannoSV kn = dsknDAO.getHoadonNoSV(mahoadon);
        float tongdv = 0;
        Assert.assertNotNull(kn);
        for (HoadonDichvu hddv : kn.getDsHoadonDichvu()) {
            tongdv += hddv.getThanhtien();
        }
        Assert.assertEquals(tongdv, tiendichvu,0.0);
    }


    @Test
    public void getHoadonNoSV_chuan15() {
        // Hoa don chưa thanh toán
        int mahoadon = 15;
        DSKhoannoSV hdn = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertEquals(0, hdn.getDsThanhtoanHoadon().size());
    }

    @Test
    public void getHoadonNoSV_chuan16() {
        // Tiền thanh toán + tiền nợ = tổng tiền ban đầu
        int mahoadon = 7;
        float tongthanhtoan = 0;
        DSKhoannoSV hdn = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(hdn);
        for (ThanhtoanHoadon tthd : hdn.getDsThanhtoanHoadon()) {
            tongthanhtoan+=tthd.getSotienThanhtoan();
        }
        Assert.assertEquals(hdn.getTienthangHientai(),tongthanhtoan+hdn.getTongnoThangHientai(), 0.0);
    }

    @Test
    public void getHoadonNoSV_chuan17() {
        // Hiển thị đúng thông tin sinh viên
        int mahoadon = 7;
        String tensinhvien = "Bảo";
        String masv = "B16DCCN003";
        DSKhoannoSV hdn = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(hdn);
        Assert.assertEquals(tensinhvien, hdn.getHopdong().getSinhvien().getHoten().getTen());
        Assert.assertEquals(masv,hdn.getHopdong().getSinhvien().getMasv());
    }

    @Test
    public void getHoadonNoSV_chuan18() {
        // Hiển thị đúng thông tin phòng
        int mahoadon = 7;
        String tenPhong = "102";
        String tenGiuong = "P102G01";
        DSKhoannoSV hdn = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(hdn);
        Assert.assertEquals(tenPhong, hdn.getHopdong().getGiuong().getPhong().getTen());
        Assert.assertEquals(tenGiuong, hdn.getHopdong().getGiuong().getTen());
    }

    @Test
    public void getHoadonNoSV_chuan19() {
        // Check tổng lịch sử thanh toán
        int mahoadon = 7;
        float tongtienthanhtoan = (float) 500000.00;
        float tong = 0;
        DSKhoannoSV hdn = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(hdn);
        for (ThanhtoanHoadon tthd : hdn.getDsThanhtoanHoadon()) {
            tong += tthd.getSotienThanhtoan();
        }
        Assert.assertEquals(tongtienthanhtoan, tong, 0.0);
    }

    @Test
    public void getHoadonNoSV_chuan20() {
        // Check sinh viên đã thanh toán -> ds thanh toán > 0
        int mahoadon = 7;
        DSKhoannoSV hdn = dsknDAO.getHoadonNoSV(mahoadon);
        Assert.assertNotNull(hdn);
        boolean check = hdn.getDsThanhtoanHoadon().size() > 0;
        Assert.assertTrue(check);
    }

}
