package com.ptit.dao;

import com.ptit.models.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DSKhoannoSVDAO extends DAO{

    public DSKhoannoSVDAO() {
        super();
    }

    public ArrayList<DSKhoannoSV> getDSKhoannoSV(String masv) {

        ArrayList<DSKhoannoSV> kq = null;
        System.out.println("MSV"+ masv);

        String sql = "CALL DSHoadonNoSV(?)";

        try {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1,masv);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                if (kq==null) kq = new ArrayList<DSKhoannoSV>();
                DSKhoannoSV dsKhoannoSV = new DSKhoannoSV();

                Hoten hoten = new Hoten();
                hoten.setHo(rs.getString("hodem"));
                hoten.setTen(rs.getString("tensinhvien"));
                Sinhvien sinhvien = new Sinhvien();
                sinhvien.setHoten(hoten);

                Phong phong = new Phong();
                phong.setTen(rs.getString("tenphong"));
                Giuong giuong = new Giuong();
                giuong.setTen(rs.getString("tengiuong"));
                giuong.setPhong(phong);

                Hopdong hopdong = new Hopdong();
                hopdong.setGiuong(giuong);
                hopdong.setSinhvien(sinhvien);

                dsKhoannoSV.setHopdong(hopdong);
                dsKhoannoSV.setId(rs.getInt("mahoadon"));
                dsKhoannoSV.setKyThanhtoan(rs.getString("kythanhtoan"));
                dsKhoannoSV.setTongnoThangHientai(rs.getFloat("tiennothanghientai"));
                dsKhoannoSV.setNgayphaiThanhtoan(rs.getDate("ngayphaithanhtoan"));
                kq.add(dsKhoannoSV);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            kq = null;
        }
        return kq;
    }

    public DSKhoannoSV getHoadonNoSV(int mahoadon) {
        DSKhoannoSV hoadonNo = null;
        PreparedStatement statement;
        ResultSet rs;
        String sqlHoadonNo = "CALL HoadonNoSV(?)";
        String sqlDSThanhtoanHoadon = "CALL DSThanhtoanHoadon(?)";
        String sqlDSHoadonDichvu = "CALL DSHoadonDichvu(?)";

        try {
            statement = con.prepareStatement(sqlHoadonNo);
            statement.setInt(1, mahoadon);
            rs = statement.executeQuery();
            while (rs.next()) {
                if (hoadonNo == null) {
                    hoadonNo = new DSKhoannoSV();
                }
                hoadonNo.setId(rs.getInt("id"));

                Hoten ht = new Hoten();
                ht.setHo(rs.getString("hodem"));
                ht.setTen(rs.getString("tensinhvien"));
                Sinhvien sv = new Sinhvien();
                sv.setHoten(ht);
                sv.setMasv(rs.getString("masv"));

                Phong phong = new Phong();
                phong.setTen(rs.getString("tenphong"));

                Giuong giuong = new Giuong();
                giuong.setTen(rs.getString("tengiuong"));
                giuong.setPhong(phong);

                Hopdong hd = new Hopdong();
                hd.setSinhvien(sv);
                hd.setGiuong(giuong);
                hd.setTienthue(rs.getFloat("giathue"));
                hoadonNo.setHopdong(hd);
                hoadonNo.setKyThanhtoan(rs.getString("kythanhtoan"));
                hoadonNo.setNgayphaiThanhtoan(rs.getDate("ngayphaithanhtoan"));
                hoadonNo.setTrangthai(rs.getInt("trangthai"));
                hoadonNo.setTienthangHientai(rs.getFloat("tienthanghientai"));
                hoadonNo.setTongnoThangHientai(rs.getFloat("tiennothanghientai"));
                hoadonNo.setDsThanhtoanHoadon(new ArrayList<ThanhtoanHoadon>());
                hoadonNo.setDsHoadonDichvu(new ArrayList<HoadonDichvu>());

                PreparedStatement stDsThanhtoanHd = con.prepareStatement(sqlDSThanhtoanHoadon);
                stDsThanhtoanHd.setInt(1,mahoadon);
                ResultSet rsDsThanhtoanHd = stDsThanhtoanHd.executeQuery();
                while (rsDsThanhtoanHd.next()) {
                    ThanhtoanHoadon tthd = new ThanhtoanHoadon();
                    tthd.setSotienThanhtoan(rsDsThanhtoanHd.getFloat("sotien"));
                    ThanhToan thanhToan = new ThanhToan();
                    thanhToan.setNgaythanhtoan(rsDsThanhtoanHd.getDate("ngaythanhtoan"));
                    tthd.setThanhToan(thanhToan);
                    hoadonNo.getDsThanhtoanHoadon().add(tthd);
                }

                PreparedStatement stDSHoadonDichvu = con.prepareStatement(sqlDSHoadonDichvu);
                stDSHoadonDichvu.setInt(1, mahoadon);
                ResultSet rsDSHoadonDichvu = stDSHoadonDichvu.executeQuery();
                while (rsDSHoadonDichvu.next()) {
                    HoadonDichvu hddv = new HoadonDichvu();
                    Dichvu dv = new Dichvu();
                    dv.setTen(rsDSHoadonDichvu.getString("tendichvu"));
                    hddv.setDichvu(dv);
                    hddv.setDongia(rsDSHoadonDichvu.getFloat("dongia"));
                    hddv.setSoluongSudung(rsDSHoadonDichvu.getInt("soluongsudung"));
                    hddv.setThanhtien(rsDSHoadonDichvu.getFloat("thanhtien"));
                    hoadonNo.getDsHoadonDichvu().add(hddv);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
            hoadonNo = null;
        }

        return hoadonNo;
    }

}
