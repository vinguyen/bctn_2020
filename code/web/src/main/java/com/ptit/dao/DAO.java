package com.ptit.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAO {

    public static Connection con;
//    private static final String HOSTNAME = "139.180.154.73";
    private static final String HOSTNAME = "localhost";
    private static final String DBNAME = "room_manage?autoReconnect=true&useSSL=false&serverTimezone=UTC";
//    private static final String USERNAME = "videv";
    private static final String USERNAME = "root";
//    private static final String PASSWORD = "Videv@067448";
    private static final String PASSWORD = "";

    public DAO() {
        if (con == null) {
            String dbUrl = "jdbc:mysql://"+HOSTNAME+":3306/"+DBNAME;

            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection(dbUrl, USERNAME, PASSWORD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException ignored) {
        }
    }

}
