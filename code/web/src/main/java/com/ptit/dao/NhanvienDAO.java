package com.ptit.dao;

import com.ptit.models.Nhanvien;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class NhanvienDAO extends DAO {

    public NhanvienDAO() {
        super();
    }

    public boolean kiemtraDangnhap(Nhanvien nhanvien) {
        boolean kq = false;

        if(nhanvien.getUsername().contains("true") ||
                nhanvien.getUsername().contains("=")||
                nhanvien.getPassword().contains("true") ||
                nhanvien.getPassword().contains("=")) return false;

        String sql = "SELECT * FROM tblnhanvien WHERE username=? AND password=?";

        try {
            PreparedStatement statement=con.prepareStatement(sql);
            statement.setString(1,nhanvien.getUsername());
            statement.setString(2, nhanvien.getPassword());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                kq = true;
            }

        }catch(Exception e){
            e.printStackTrace();
            kq = false;
        }

        return kq;
    }

    public static void main(String[] args) {
        Nhanvien nv = new Nhanvien("vinguyen","123456");

        NhanvienDAO nhanvienDAO = new NhanvienDAO();

        System.out.println(nhanvienDAO.kiemtraDangnhap(nv));
    }

}
