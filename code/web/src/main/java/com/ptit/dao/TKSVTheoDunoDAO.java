package com.ptit.dao;

import com.ptit.models.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TKSVTheoDunoDAO extends DAO {

    public TKSVTheoDunoDAO() {
        super();
    }

    public ArrayList<TKSVTheoDuno> getTKSVTheoDuno() {

        ArrayList<TKSVTheoDuno> kq = null;

        String sql = "CALL TKSVTheoDuno";

        try {
            PreparedStatement statement = con.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                if(kq==null) kq = new ArrayList<TKSVTheoDuno>();

                TKSVTheoDuno tksvTheoDuno = new TKSVTheoDuno();

                Hoten ht = new Hoten();
                ht.setHo(rs.getString("hodem"));
                ht.setTen(rs.getString("ten"));

                Sinhvien sv = new Sinhvien();
                sv.setId(rs.getInt("id"));
                sv.setMasv(rs.getString("masv"));
                sv.setHoten(ht);
                sv.setEmail(rs.getString("email"));
                sv.setSodt(rs.getString("dienthoai"));
                sv.setSocmt(rs.getString("socmt"));

                Truong truong = new Truong();
                truong.setTen(rs.getString("tentruong"));
                Khoa khoa = new Khoa();
                khoa.setTen(rs.getString("tenkhoa"));
                khoa.setTruong(truong);

                tksvTheoDuno.setSinhvien(sv);
                tksvTheoDuno.setKhoa(khoa);
                tksvTheoDuno.setNienkhoa(rs.getString("nienkhoa"));
                Phong phong = new Phong();
                phong.setTen(rs.getString("tenphong"));
                phong.setCoDieuhoa(rs.getBoolean("codieuhoa"));
                phong.setSucchua(rs.getInt("succhua"));
                Giuong giuong = new Giuong();
                giuong.setTen(rs.getString("tengiuong"));
                giuong.setVitri(rs.getBoolean("kieugiuong"));
                giuong.setPhong(phong);

                tksvTheoDuno.setGiuong(giuong);
                tksvTheoDuno.setTongtienno(rs.getFloat("tongtienno"));
                kq.add(tksvTheoDuno);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            kq = null;
        }

        return kq;
    }

}


