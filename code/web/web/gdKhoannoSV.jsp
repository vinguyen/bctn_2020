<%@ page import="java.util.ArrayList" %>
<%@ page import="com.ptit.models.DSKhoannoSV" %>
<%@ page import="com.ptit.dao.DSKhoannoSVDAO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String masv = (String)request.getParameter("masv");
    ArrayList<DSKhoannoSV> dsKhoannoSVS = (new DSKhoannoSVDAO()).getDSKhoannoSV(masv);
    float tongtienno = 0;
    int tongKhoanno = 0;
%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Danh sách khoản nợ của sinh viên</title>
    <%@include file="components/header.jsp" %>
</head>
<body>
<%@include file="components/auth.jsp" %>
<div class="contain-common">
    <div class="main-nav">
        <div class="container contain-top">
            <div class="nav-left">
                <a href="gdChinhQL.jsp">
                    <img src="https://image.flaticon.com/icons/png/512/262/262815.png"
                         alt="Hệ thống quản lý ký túc xá"
                    >
                </a>
            </div>
            <div class="nav-right">
                <div class="btn-group">
                    <button type="button"
                            class="btn btn-info dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <% if (nv!=null) { %>
                        <%= nv.getUsername()%>
                        <% } %>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="doDangxuat.jsp">Đăng xuất</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-thong-ke">
        <div class="container">
            <%
                if(dsKhoannoSVS!=null) {
            %>
            <h3 class="title-chon-tk">Danh sách các khoản nợ của sinh viên <%= dsKhoannoSVS.get(0).getHopdong().getSinhvien().getHoten().getHo() + " " + dsKhoannoSVS.get(0).getHopdong().getSinhvien().getHoten().getTen() %></h3>
            <div class="info-hoadon">
                <table class="table table-info-hoadon">
                    <tr>
                        <th>Mã sinh viên:</th>
                        <td>
                            <%= masv %>
                        </td>
                    </tr>
                    <tr>
                        <th>Tên sinh viên:</th>
                        <td>
                            <%= dsKhoannoSVS.get(0).getHopdong().getSinhvien().getHoten().getHo() + " " + dsKhoannoSVS.get(0).getHopdong().getSinhvien().getHoten().getTen() %>
                        </td>
                    </tr>
                    <tr>
                        <th>Mã giường:</th>
                        <td>
                            <%= dsKhoannoSVS.get(0).getHopdong().getGiuong().getTen() %>
                            - Phòng <%= dsKhoannoSVS.get(0).getHopdong().getGiuong().getPhong().getTen() %>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tên khoản nợ</th>
                            <th scope="col">Ngày phải thanh toán</th>
                            <th scope="col">Tổng tiền (VNĐ)</th>
                        </tr>
                    </thead>
                    <tbody>
                    <%
                        for (DSKhoannoSV hd : dsKhoannoSVS) {
                            tongtienno+= hd.getTongnoThangHientai();
                            tongKhoanno++;
                    %>

                    <tr onclick="openPage('gdChitietKhoannoSV.jsp?mahoadon=<%=hd.getId()%>')">
                        <td>
                            <%=tongKhoanno%>
                        </td>
                        <td>
                            Hóa đơn <%= hd.getKyThanhtoan() %>
                        </td>
                        <td>
                            <%= hd.getNgayphaiThanhtoan()%>
                        </td>
                        <td>
                            <%= String.format("%,.2f", hd.getTongnoThangHientai())%>
                        </td>
                    </tr>

                    <%
                        }
                    %>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="3">Tổng cộng:</th>
                        <th>
                            <%= String.format("%,.2f", tongtienno)%>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <%
                }
            %>
        </div>
        <div class="main-footer">
            <button type="button" onclick="goBack()" class="btn btn-secondary">Quay lại</button>
        </div>
    </div>
</div>

</body>
</html>
