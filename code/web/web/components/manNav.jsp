<%@include file="auth.jsp" %>
<div class="main-nav">
    <div class="container contain-top">
        <div class="nav-left">
            <a href="gdChinhQL.jsp">
                <img src="https://image.flaticon.com/icons/png/512/262/262815.png"
                     alt="Hệ thống quản lý ký túc xá"
                >
            </a>
        </div>
        <div class="nav-right">
            <div>
                <a href="" class="link-page">Trang chủ</a>
                <a href="#" class="link-page">Quản lý phòng</a>
                <a href="gdChonTK.jsp" class="link-page">Xem thống kê</a>
            </div>
            <div class="btn-group">
                <button type="button"
                        class="btn btn-info dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                    <% if (nv!=null) { %>
                    <%= nv.getUsername()%>
                    <% } %>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="doDangxuat.jsp">Đăng xuất</a>
                </div>
            </div>
        </div>
    </div>
</div>
