<%@ page import="com.ptit.models.DSKhoannoSV" %>
<%@ page import="com.ptit.dao.DSKhoannoSVDAO" %>
<%@ page import="com.ptit.models.HoadonDichvu" %>
<%@ page import="com.ptit.models.ThanhtoanHoadon" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    System.out.println();
    int mahoadon = -1;
    if(request.getParameter("mahoadon")!=null) {
        mahoadon = Integer.parseInt(request.getParameter("mahoadon"));
    }
    DSKhoannoSV hoadonNo = (new DSKhoannoSVDAO()).getHoadonNoSV(mahoadon);
    float tongtienno = 0;
    int index = 0;
    float tongtienDichvu = 0;
    int indexThanhtoan = 0;
    float tongtienThanhtoan = 0;
    if (hoadonNo != null) {
        for (HoadonDichvu hoadonDichvu : hoadonNo.getDsHoadonDichvu()) {
            tongtienDichvu+= hoadonDichvu.getThanhtien();
        }
        for (ThanhtoanHoadon thanhtoanHoadon : hoadonNo.getDsThanhtoanHoadon()) {
            tongtienThanhtoan+=thanhtoanHoadon.getSotienThanhtoan();
        }
    }
%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thông tin khoản nợ chi tiết</title>
    <%@include file="components/header.jsp" %>
</head>
<body>
<%@include file="components/auth.jsp" %>
<div class="contain-common">
    <div class="main-nav">
        <div class="container contain-top">
            <div class="nav-left">
                <a href="gdChinhQL.jsp">
                    <img src="https://image.flaticon.com/icons/png/512/262/262815.png"
                         alt="Hệ thống quản lý ký túc xá"
                    >
                </a>
            </div>
            <div class="nav-right">
                <div class="btn-group">
                    <button type="button"
                            class="btn btn-info dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <% if (nv!=null) { %>
                        <%= nv.getUsername()%>
                        <% } %>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="doDangxuat.jsp">Đăng xuất</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-thong-ke">
        <div class="contain-hoa-don">
            <%
                if(hoadonNo!=null) {
            %>
            <h3 class="title-chon-tk">Thông tin chi tiết khoản nợ </h3>
            <div class="info-hoadon">
                <div class="row">
                    <div class="col-sm-6">
                        <table class="table table-info-hoadon">
                            <tr>
                                <th>
                                    Mã hóa đơn:
                                </th>
                                <td>
                                    <%= hoadonNo.getId() %>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Kỳ thanh toán:
                                </th>
                                <td>
                                    <%= hoadonNo.getKyThanhtoan() %>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Mã sinh viên:
                                </th>
                                <td>
                                    <%= hoadonNo.getHopdong().getSinhvien().getMasv()%>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Tên sinh viên:
                                </th>
                                <td>
                                    <%= hoadonNo.getHopdong().getSinhvien().getHoten().getHo() + " " + hoadonNo.getHopdong().getSinhvien().getHoten().getTen() %>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Mã giường:
                                </th>
                                <td>
                                    <%= hoadonNo.getHopdong().getGiuong().getTen() %>
                                    - Phòng <%= hoadonNo.getHopdong().getGiuong().getPhong().getTen() %>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <table class="table table-info-hoadon">
                            <tr>
                                <th>
                                    Tiền phòng:
                                </th>
                                <td>
                                    <%= String.format("%,.2f", hoadonNo.getHopdong().getTienthue()) %>
                                    VNĐ
                                </td>
                            </tr>
                            <tr class="tr-tong-tien">
                                <th>
                                    Tiền dịch vụ:
                                </th>
                                <td>
                                    <%= String.format("%,.2f", tongtienDichvu)%>
                                    VNĐ
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Tổng tiền:
                                </th>
                                <th>
                                    <%= String.format("%,.2f", hoadonNo.getTienthangHientai()) %>
                                    VNĐ
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    Đã thanh toán:
                                </th>
                                <th>
                                    <%= String.format("%,.2f", tongtienThanhtoan) %>
                                    VNĐ
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    Còn lại:
                                </th>
                                <th>
                                    <%= String.format("%,.2f", hoadonNo.getTongnoThangHientai()) %>
                                    VNĐ
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="info-hoadon-dichvu">
                <h3>Dịch vụ sử dụng</h3>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên dịch vụ</th>
                            <th>Số lượng sử dụng</th>
                            <th>Đơn giá (VNĐ)</th>
                            <th>Thành tiền (VNĐ)</th>
                        </tr>
                        </thead>
                        <%
                            if (hoadonNo.getDsHoadonDichvu().size() > 0) {
                        %>
                        <tbody>
                        <%
                            for (HoadonDichvu hoadonDichvu : hoadonNo.getDsHoadonDichvu()) {
                                index++;
                        %>
                        <tr>
                            <td>
                                <%=index%>
                            </td>
                            <th>
                                <%=hoadonDichvu.getDichvu().getTen()%>
                            </th>
                            <td>
                                <%=hoadonDichvu.getSoluongSudung()%>
                            </td>
                            <td>
                                <%=String.format("%,.2f", hoadonDichvu.getDongia())%>
                            </td>
                            <td>
                                <%= String.format("%,.2f", hoadonDichvu.getThanhtien())%>
                            </td>
                        </tr>
                        <%
                            }

                        %>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">Tổng cộng</th>
                            <th>
                                <%= String.format("%,.2f", tongtienDichvu)%>
                            </th>
                        </tr>
                        </tfoot>
                        <%
                        }
                        else {
                        %>
                        <tbody>
                        <tr>
                            <td colspan="5">Không có dữ liệu</td>
                        </tr>
                        </tbody>
                        <%
                            }
                        %>
                    </table>
                </div>
            </div>

            <div class="info-hoadon-dichvu">
                <h3>Lịch sử thanh toán</h3>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ngày thanh toán</th>
                            <th>Số tiền (VNĐ)</th>
                        </tr>
                        </thead>
                        <%
                            if(hoadonNo.getDsThanhtoanHoadon().size()>0) {
                        %>
                        <tbody>
                        <%
                            for (ThanhtoanHoadon thanhtoanHoadon : hoadonNo.getDsThanhtoanHoadon()) {
                                indexThanhtoan++;
                        %>
                        <tr>
                            <td>
                                <%=indexThanhtoan%>
                            </td>
                            <td>
                                <%=thanhtoanHoadon.getThanhToan().getNgaythanhtoan()%>
                            </td>
                            <td>
                                <%=String.format("%,.2f", thanhtoanHoadon.getSotienThanhtoan())%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="2">Tổng cộng</th>
                            <th>
                                <%= String.format("%,.2f", tongtienThanhtoan)%>
                            </th>
                        </tr>
                        </tfoot>
                        <%
                            } else {
                        %>
                        <tbody>
                            <tr>
                                <td colspan="3">Không có dữ liệu</td>
                            </tr>
                        </tbody>
                        <%
                            }
                        %>

                    </table>
                </div>
            </div>
            <%
                }
            %>
        </div>
        <div class="main-footer">
            <button type="button" onclick="goBack()" class="btn btn-secondary">Quay lại</button>
        </div>
    </div>
</div>
</body>
</html>
