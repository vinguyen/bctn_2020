<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="com.ptit.models.*"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hệ thống quản lý giường KTX</title>
    <%@include file="components/header.jsp" %>
</head>
<body>
<%@include file="components/auth.jsp" %>
<div class="contain-common">
    <div class="main-nav">
        <div class="container contain-top">
            <div class="nav-left">
                <a href="gdChinhQL.jsp">
                    <img src="https://image.flaticon.com/icons/png/512/262/262815.png"
                         alt="Hệ thống quản lý ký túc xá"
                    >
                </a>
            </div>
            <div class="nav-right">
                <div class="option-home">
                    <a href="" class="link-page">Trang chủ</a>
                    <a href="#" class="link-page">Quản lý phòng</a>
                    <a href="gdChonTK.jsp" class="link-page">Xem thống kê</a>
                </div>
                <div class="btn-group">
                    <button type="button"
                            class="btn btn-info dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <% if (nv!=null) { %>
                        <%= nv.getUsername()%>
                        <% } %>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item active-mobile" href="#">Quản lý phòng</a>
                        <a class="dropdown-item active-mobile" href="gdChonTK.jsp">Xem thống kê</a>
                        <a class="dropdown-item" href="doDangxuat.jsp">Đăng xuất</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-body main-ql">
        <h3 class="title-page">
            Hệ thống quản lý ký túc xá
        </h3>
    </div>
</div>

<%--<h2> Trang chủ quản lý </h2>--%>
<%--<button onclick="openPage('gdChonTK.jsp')">Xem thống kê</button>--%>
</body>
</html>
