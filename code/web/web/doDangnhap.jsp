<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="java.util.ArrayList,com.ptit.dao.*,com.ptit.models.*"%>
<%
    String username = (String)request.getParameter("username");
    String password = (String)request.getParameter("password");
    Nhanvien nv = new Nhanvien();
    nv.setUsername(username);
    nv.setPassword(password);
    NhanvienDAO nhanvienDAO = new NhanvienDAO();
    boolean kq = nhanvienDAO.kiemtraDangnhap(nv);
    if(kq){
        session.setAttribute("nhanvien", nv);
        response.sendRedirect("gdChinhQL.jsp");
    }
    else{
        response.sendRedirect("gdDangnhap.jsp?err=fail");
    }
%>
