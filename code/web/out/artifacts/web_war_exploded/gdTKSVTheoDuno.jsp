<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         import="com.ptit.models.*"
%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.ptit.dao.TKSVTheoDunoDAO" %>

<%
    ArrayList<TKSVTheoDuno> tksvTheoDunos = (new TKSVTheoDunoDAO()).getTKSVTheoDuno();
    float tongtienno = 0;
    int tongsinhvien = 0;
%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thống kê sinh viên theo dư nợ</title>
    <%@include file="components/header.jsp" %>
</head>
<body>
<%@include file="components/auth.jsp" %>
<div class="contain-common">
    <div class="main-nav">
        <div class="container contain-top">
            <div class="nav-left">
                <a href="gdChinhQL.jsp">
                    <img src="https://image.flaticon.com/icons/png/512/262/262815.png"
                         alt="Hệ thống quản lý ký túc xá"
                    >
                </a>
            </div>
            <div class="nav-right">
                <div class="btn-group">
                    <button type="button"
                            class="btn btn-info dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <% if (nv!=null) { %>
                        <%= nv.getUsername()%>
                        <% } %>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="doDangxuat.jsp">Đăng xuất</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-thong-ke">
        <div class="container">

            <h3 class="title-chon-tk">Thống kê sinh viên theo dư nợ</h3>
            <div class="table-responsive">
                <%
                    if(tksvTheoDunos!=null) {
                %>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">MSV</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Số CMT</th>
                        <th scope="col">Số ĐT</th>
                        <th scope="col">Trường</th>
                        <th scope="col">Khoa</th>
                        <th scope="col">Niên khóa</th>
                        <th scope="col">Tên phòng</th>
                        <th scope="col">Kiểu phòng</th>
                        <th scope="col">Mã giường</th>
                        <th scope="col">Loại giường</th>
                        <th scope="col">Tổng số tiền nợ (VNĐ)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        for (TKSVTheoDuno tk: tksvTheoDunos) {
                            tongtienno+= tk.getTongtienno();
                            tongsinhvien++;
                    %>
                    <tr onclick="openPage('gdKhoannoSV.jsp?masv=<%=tk.getSinhvien().getMasv()%>')">
                        <td>
                            <%= tongsinhvien %>
                        </td>
                        <td>
                            <%= tk.getSinhvien().getMasv() %>
                        </td>
                        <td>
                            <%= tk.getSinhvien().getHoten().getHo() + " " + tk.getSinhvien().getHoten().getTen() %>
                        </td>
                        <td>
                            <%= tk.getSinhvien().getSocmt() %>
                        </td>
                        <td>
                            <%= tk.getSinhvien().getSodt() %>
                        </td>
                        <td>
                            <%= tk.getKhoa().getTruong().getTen() %>
                        </td>
                        <td>
                            <%= tk.getKhoa().getTen() %>
                        </td>
                        <td>
                            <%= tk.getNienkhoa() %>
                        </td>
                        <td>
                            <%= tk.getGiuong().getPhong().getTen() %>
                        </td>
                        <td>
                            <%= tk.getGiuong().getPhong().getSucchua() + " giường, " %>
                            <%= (tk.getGiuong().getPhong().isCoDieuhoa()?"Có":"Không") + " điều hòa" %>
                        </td>
                        <td>
                            <%= tk.getGiuong().getTen() %>
                        </td>
                        <td>
                            <%= tk.getGiuong().isVitri()?"Tầng trên":"Tầng dưới"%>
                        </td>
                        <td>
                            <%= String.format("%,.2f", tk.getTongtienno())%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="12">Tổng cộng: </th>
                        <th>
                            <%= String.format("%,.2f", tongtienno) %>
                        </th>
                    </tr>
                    </tfoot>
                </table>
                <%
                    }
                %>
            </div>
        </div>
        <div class="main-footer">
            <button type="button" onclick="goBack()" class="btn btn-secondary">Quay lại</button>
        </div>
    </div>
</div>
</body>
</html>