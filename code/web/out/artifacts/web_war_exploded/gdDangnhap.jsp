<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng nhập hệ thống</title>
    <%@include file="components/header.jsp" %>
</head>
<body class="login-page">
    <div class="container-login">
        <div class="login-logo">
            <img src="https://image.flaticon.com/icons/png/512/262/262815.png" alt="">
            <span>Hệ thống quản lý ký túc xá</span>
        </div>
        <div class="login-box-body">
            <h3 class="login-box-msg">Đăng nhập hệ thống</h3>
            <br>
            <div>
                <%
                    if(request.getParameter("err") !=null &&
                            request.getParameter("err").equalsIgnoreCase("timeout"))
                    {
                %>
                    <div class="alert alert-warning" role="alert">
                        Hết phiên làm việc. Làm ơn đăng nhập lại!
                    </div>
                <%
                    }
                    else if(request.getParameter("err") !=null &&
                        request.getParameter("err").equalsIgnoreCase("fail"))
                    {
                %>
                    <div class="alert alert-danger" role="alert">
                        Sai tên đăng nhập/mật khẩu!
                    </div>
                <%
                    }
                %>

                <form name="dangnhap" action="doDangnhap.jsp" method="post">
                    <div class="form-group row">
                        <label for="username" class="col-sm-4 col-form-label">Tên đăng nhập</label>
                        <div class="col-sm-8">
                            <input type="text"
                                   class="form-control"
                                   id="username"
                                   value="vinguyen"
                                   required
                                   placeholder="Tên đăng nhập"
                                   name="username"
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label">Mật khẩu</label>
                        <div class="col-sm-8">
                            <input type="password"
                                   class="form-control"
                                   id="password"
                                   value="123456"
                                   name="password"
                                   required
                                   placeholder="Mật khẩu"
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-primary">Đăng nhập</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
