<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chọn loại thống kê</title>
    <%@include file="components/header.jsp" %>
</head>
<body>
<%@include file="components/auth.jsp" %>
<div class="contain-common">
    <div class="main-nav">
        <div class="container contain-top">
            <div class="nav-left">
                <a href="gdChinhQL.jsp">
                    <img src="https://image.flaticon.com/icons/png/512/262/262815.png"
                         alt="Hệ thống quản lý ký túc xá"
                    >
                </a>
            </div>
            <div class="nav-right">
                <div class="btn-group">
                    <button type="button"
                            class="btn btn-info dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <% if (nv!=null) { %>
                        <%= nv.getUsername()%>
                        <% } %>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="doDangxuat.jsp">Đăng xuất</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-thong-ke">
        <div class="contain-chon-tk">
            <h3 class="title-chon-tk">Chọn loại thống kê</h3>
            <div class="ds-loat-tk">
                <a href="#">
                    <button type="button" class="btn btn-lg btn-outline-primary">
                        Thống kê doanh thu
                    </button>
                </a>
                <a href="gdTKSVTheoDuno.jsp">
                    <button type="button" class="btn btn-lg btn-outline-primary">
                        Thống kê sinh viên theo dư nợ
                    </button>
                </a>
                <a href="#">
                    <button type="button" class="btn btn-lg btn-outline-primary">
                        Thống kê các loại giường
                    </button>
                </a>
            </div>
        </div>
        <div class="main-footer">
            <button type="button" onclick="goBack()" class="btn btn-secondary">Quay lại</button>
        </div>
    </div>

</div>

</body>
</html>
