/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : room_manage

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 05/01/2021 00:28:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbldiachi
-- ----------------------------
DROP TABLE IF EXISTS `tbldiachi`;
CREATE TABLE `tbldiachi`  (
  `id` int(11) NOT NULL,
  `sonha` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `xompho` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phuongxa` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quanhuyen` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tinhthanh` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbldichvu
-- ----------------------------
DROP TABLE IF EXISTS `tbldichvu`;
CREATE TABLE `tbldichvu`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `dongia` float(255, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbldichvuphong
-- ----------------------------
DROP TABLE IF EXISTS `tbldichvuphong`;
CREATE TABLE `tbldichvuphong`  (
  `id` int(11) NOT NULL,
  `sodau` int(11) NULL DEFAULT NULL,
  `socuoi` int(11) NULL DEFAULT NULL,
  `soluongPhongSudung` int(11) NULL DEFAULT NULL,
  `tblPhongId` int(11) NOT NULL,
  `tblDichvuId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblgiuong
-- ----------------------------
DROP TABLE IF EXISTS `tblgiuong`;
CREATE TABLE `tblgiuong`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vitri` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `giathue` float(255, 0) NULL DEFAULT NULL,
  `tblPhongId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblhoadon
-- ----------------------------
DROP TABLE IF EXISTS `tblhoadon`;
CREATE TABLE `tblhoadon`  (
  `id` int(11) NOT NULL,
  `tblHopdongId` int(11) NOT NULL,
  `tienthanghientai` float(255, 0) NOT NULL,
  `tongnocu` float(255, 0) NULL DEFAULT NULL,
  `tongtien` float(255, 0) NOT NULL,
  `tongtienSVThanhtoan` float(255, 0) NULL DEFAULT NULL,
  `trangthai` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblhoadondichvu
-- ----------------------------
DROP TABLE IF EXISTS `tblhoadondichvu`;
CREATE TABLE `tblhoadondichvu`  (
  `id` int(11) NOT NULL,
  `tblHopdongDichvuId` int(11) NOT NULL,
  `soluongSudung` int(11) NOT NULL,
  `thanhtien` float(255, 0) NOT NULL,
  `tblHoadonId` int(11) NULL DEFAULT NULL,
  `tblDichvuPhongId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblhopdong
-- ----------------------------
DROP TABLE IF EXISTS `tblhopdong`;
CREATE TABLE `tblhopdong`  (
  `id` int(11) NOT NULL,
  `tiencoc` float(255, 0) NOT NULL,
  `giathue` float(255, 0) NOT NULL,
  `ngaybatdau` date NOT NULL,
  `tblSinhvienKhoaId` int(11) NOT NULL,
  `tblNhanvienId` int(11) NOT NULL,
  `tblGiuongId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblhopdongdichvu
-- ----------------------------
DROP TABLE IF EXISTS `tblhopdongdichvu`;
CREATE TABLE `tblhopdongdichvu`  (
  `id` int(11) NOT NULL,
  `tblDichvuId` int(11) NOT NULL,
  `tblHopdongId` int(11) NOT NULL,
  `soluong` int(11) NULL DEFAULT NULL,
  `dongia` float(255, 0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblkhoa
-- ----------------------------
DROP TABLE IF EXISTS `tblkhoa`;
CREATE TABLE `tblkhoa`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblTruongId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblnguoi
-- ----------------------------
DROP TABLE IF EXISTS `tblnguoi`;
CREATE TABLE `tblnguoi`  (
  `id` int(11) NOT NULL,
  `hodem` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dienthoai` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ghichu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblDiachiId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblnhanvien
-- ----------------------------
DROP TABLE IF EXISTS `tblnhanvien`;
CREATE TABLE `tblnhanvien`  (
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tblThanhvienId` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblnhanvien
-- ----------------------------
INSERT INTO `tblnhanvien` VALUES ('admin', '123456', 1);

-- ----------------------------
-- Table structure for tblphong
-- ----------------------------
DROP TABLE IF EXISTS `tblphong`;
CREATE TABLE `tblphong`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `succhua` tinyint(3) NOT NULL,
  `coDieuhoa` tinyint(3) NOT NULL,
  `ghichu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblTohopId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblsinhvien
-- ----------------------------
DROP TABLE IF EXISTS `tblsinhvien`;
CREATE TABLE `tblsinhvien`  (
  `masv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tblNguoiId` int(11) NOT NULL,
  PRIMARY KEY (`tblNguoiId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblsinhvienkhoa
-- ----------------------------
DROP TABLE IF EXISTS `tblsinhvienkhoa`;
CREATE TABLE `tblsinhvienkhoa`  (
  `id` int(11) NOT NULL,
  `nienkhoa` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `danghoc` tinyint(3) NOT NULL,
  `tblSinhvienId` int(11) NOT NULL,
  `tblKhoaId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblthanhtoanhoadon
-- ----------------------------
DROP TABLE IF EXISTS `tblthanhtoanhoadon`;
CREATE TABLE `tblthanhtoanhoadon`  (
  `id` int(11) NOT NULL,
  `sotien` float(255, 0) NOT NULL,
  `tblHoadonId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbltohop
-- ----------------------------
DROP TABLE IF EXISTS `tbltohop`;
CREATE TABLE `tbltohop`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblDiachiId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbltruong
-- ----------------------------
DROP TABLE IF EXISTS `tbltruong`;
CREATE TABLE `tbltruong`  (
  `id` int(11) NOT NULL,
  `ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `tblDiachiId` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
